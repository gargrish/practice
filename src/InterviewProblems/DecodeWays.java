package InterviewProblems;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DecodeWays {
    public static int numDecodings(String s) {
        Map<String, Set<String>> validCombinationsMap = new HashMap<>();
        numDecodingsUtil(s, validCombinationsMap);
        return validCombinationsMap.get(s).size();
    }

    public static void numDecodingsUtil(String s, Map<String, Set<String>> validCombinationsMap) {

        if (validCombinationsMap.containsKey(s)) {
            return;
        }

        Set<String> currentCombinations = new HashSet<>();

        if (!s.startsWith("0") && s.length() <= 2 && Integer.parseInt(s) >= 1 && Integer.parseInt(s) <= 26) {
            currentCombinations.add(s);
        }

        for (int i = 0; i < s.length(); i++) {
            String part1 = s.substring(0, i + 1);
            String part2 = s.substring(i + 1);
            if (!part1.startsWith("0") && part1.length() <= 2 && Integer.parseInt(part1) >= 1 && Integer.parseInt(part1) <= 26) {
                if (part2.isEmpty()) {
                    currentCombinations.add(part1);
                } else {
                    numDecodingsUtil(part2, validCombinationsMap);
                    for (String str : validCombinationsMap.get(part2)) {
                        currentCombinations.add(part1 + " " + str);
                    }
                }
            } else if (!part2.isEmpty() && !part2.startsWith("0") && part2.length() <= 2 && Integer.parseInt(part2) >= 1 && Integer.parseInt(part2) <= 26) {
                if (part1.isEmpty()) {
                    currentCombinations.add(part2);
                } else {
                    numDecodingsUtil(part1, validCombinationsMap);
                    for (String str : validCombinationsMap.get(part1)) {
                        currentCombinations.add(str + " " + part2);
                    }
                }
            }
        }
        validCombinationsMap.put(s, currentCombinations);
    }

    public static void main(String[] args) {
        System.out.println(numDecodings("4757562545844617494555774581341211511296816786586787755257741178599337186486723247528324612117156948"));
    }
}
