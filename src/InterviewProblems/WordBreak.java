package InterviewProblems;

import java.util.List;

/*
Word Break
Example 1:

Input: s = "leetcode", wordDict = ["leet", "code"]
Output: true
Explanation: Return true because "leetcode" can be segmented as "leet code".
Example 2:

Input: s = "applepenapple", wordDict = ["apple", "pen"]
Output: true
Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
             Note that you are allowed to reuse a dictionary word.
 */

public class WordBreak {
    public boolean wordBreak(String s, List<String> wordDict) {
        Boolean[] isPresent = new Boolean[s.length()];
        return wordUtil(s, 0, wordDict, isPresent);
    }

    public boolean wordUtil(String s, int start, List<String> wordDict, Boolean[] isPresent) {
        if (isPresent[start] != null) {
            return isPresent[start];
        }

        if (wordDict.contains(s)) {
            isPresent[start] = true;
            return true;
        }
        for (int i = 0; i < s.length(); i++) {
            String part1 = s.substring(0, i + 1);
            String part2 = s.substring(i + 1);
            if (wordDict.contains(part1) && wordUtil(part2, i + 1, wordDict, isPresent)) {
                isPresent[start] = true;
                return true;
            }
        }
        isPresent[start] = false;
        return false;
    }

    public static void main(String[] args) {
    }
}
