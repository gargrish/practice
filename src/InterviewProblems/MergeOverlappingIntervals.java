package InterviewProblems;

import java.util.*;

/*
Input: [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
 */
public class MergeOverlappingIntervals {
    static class Interval {
        int start;
        int end;

        Interval() {

        }

        Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public static int[][] merge(int[][] intervals) {
        List<Interval> intervalsList = new ArrayList<>();
        for (int i = 0; i < intervals.length; i++) {
            intervalsList.add(new Interval(intervals[i][0], intervals[i][1]));
        }
        List<Interval> intervalsListSortStart = new ArrayList<>(intervalsList);
        List<Interval> intervalsListSortEnd = new ArrayList<>(intervalsList);
        Collections.sort(intervalsListSortStart, (o1, o2) -> {
            if (o1.start < o2.start) {
                return -1;
            }
            if (o1.start > o2.start) {
                return 1;
            }
            return 0;
        });
        Collections.sort(intervalsListSortEnd, (o1, o2) -> {
            if (o1.end < o2.end) {
                return -1;
            }
            if (o1.end > o2.end) {
                return 1;
            }
            return 0;
        });
        int i = 0, j = 0;
        List<Interval> temp = new ArrayList<>();
        List<Interval> output = new ArrayList<>();
        Interval out;
        while (i < intervalsListSortStart.size() && j < intervalsListSortEnd.size()) {
            if (intervalsListSortStart.get(i).start <= intervalsListSortEnd.get(j).end) {
                if (output.size() > 0 && doIntersect(output.get(output.size() - 1), intervalsListSortStart.get(i))) {
                    temp.add(output.get(output.size() - 1));
                    temp.add(intervalsListSortStart.get(i));
                    output.remove(output.size() - 1);
                    out = getMaxInterval(temp);
                    if (out != null) {
                        output.add(out);
                    }
                    temp.clear();
                } else {
                    temp.add(intervalsListSortStart.get(i));
                }
                i++;
            } else {
                out = getMaxInterval(temp);
                if (out != null) {
                    output.add(out);
                }
                temp.clear();
                j++;
            }
        }
        if (temp.size() > 0) {
            out = getMaxInterval(temp);
            if (out != null) {
                output.add(out);
            }
        }
        int[][] response = new int[output.size()][2];
        i = 0;
        for (Interval interval : output) {
            response[i++] = new int[]{interval.start, interval.end};
        }
        return response;
    }

    private static boolean doIntersect(Interval i1, Interval i2) {
        if ((i1.start >= i2.start && i1.start <= i2.end) || (i1.end >= i2.start && i1.end <= i2.end) ||
                (i2.start >= i1.start && i2.start <= i1.end) || (i2.end >= i1.start && i2.end <= i1.end)) {
            return true;
        }
        return false;
    }

    private static Interval getMaxInterval(List<Interval> list) {
        if (list.size() == 0) {
            return null;
        }
        int startMin = list.get(0).start;
        int endMax = list.get(0).end;
        for (int i = 1; i < list.size(); i++) {
            startMin = Math.min(startMin, list.get(i).start);
            endMax = Math.max(endMax, list.get(i).end);
        }
        return new Interval(startMin, endMax);
    }

    public static void main(String[] args) {
        //[[1,3],[2,6],[8,10],[15,18]]
        int[][] input2 = {
                {1, 4},
                {0, 2},
                {3, 5},
                {5, 10},
                {11, 15},
        };
        int[][] input = {
                {2, 3},
                {4, 5},
                {6, 7},
                {8, 9},
                {1, 10},
        };
        for (int i = 0; i < merge(input).length; i++) {
            for (int j = 0; j < merge(input)[i].length; j++) {
                System.out.println(merge(input)[i][j]);
            }
        }
        for (int i = 0; i < merge(input2).length; i++) {
            for (int j = 0; j < merge(input2)[i].length; j++) {
                System.out.println(merge(input2)[i][j]);
            }
        }
    }
}
