package InterviewProblems;

/*
Number of Islands
Example 1:

Input:
11110
11010
11000
00000

Output: 1
Example 2:

Input:
11000
11000
00100
00011

Output: 3
 */

public class NumberOfIslands {
    public static int numIslands(char[][] grid) {
        if (grid.length == 0) {
            return 0;
        }
        int i, j;
        int count = 0;
        boolean[][] hasBeenVisited = new boolean[grid.length][grid[0].length];
        for (i = 0; i < grid.length; i++) {
            for (j = 0; j < grid[0].length; j++) {
                if (!hasBeenVisited[i][j] && grid[i][j] == '1') {
                    hasBeenVisited[i][j] = true;
                    visitLand(grid, hasBeenVisited, i, j);
                    count++;
                }
            }
        }
        return count;
    }

    public static void visitLand(char[][] grid, boolean[][] hasBeenVisited, int i, int j) {
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length) {
            return;
        }
        if (!hasBeenVisited[i][j] && grid[i][j] == '1') {
            hasBeenVisited[i][j] = true;
        }
        if (hasBeenVisited[i][j] && i + 1 >= 0 && i + 1 < grid.length && j >= 0 && j < grid[0].length && !hasBeenVisited[i + 1][j]) {
            visitLand(grid, hasBeenVisited, i + 1, j);
        }
        if (hasBeenVisited[i][j] && i - 1 >= 0 && i - 1 < grid.length && j >= 0 && j < grid[0].length && !hasBeenVisited[i - 1][j]) {
            visitLand(grid, hasBeenVisited, i - 1, j);
        }
        if (hasBeenVisited[i][j] && i >= 0 && i < grid.length && j + 1 >= 0 && j + 1 < grid[0].length && !hasBeenVisited[i][j + 1]) {
            visitLand(grid, hasBeenVisited, i, j + 1);
        }
        if (hasBeenVisited[i][j] && i >= 0 && i < grid.length && j - 1 >= 0 && j - 1 < grid[0].length && !hasBeenVisited[i][j - 1]) {
            visitLand(grid, hasBeenVisited, i, j - 1);
        }
    }

    public static void main(String[] args) {
        char[][] grid = {
                {'1', '1', '1', '1', '0'},
                {'1', '1', '0', '1', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '0', '0', '0'},
        };
        System.out.println(numIslands(grid));
    }
}
