package InterviewProblems;

import java.util.*;

/*
Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, add spaces in s to construct a sentence where each word is a valid dictionary word. Return all such possible sentences.

Note:

The same word in the dictionary may be reused multiple times in the segmentation.
You may assume the dictionary does not contain duplicate words.
Example 1:

Input:
s = "catsanddog"
wordDict = ["cat", "cats", "and", "sand", "dog"]
Output:
[
  "cats and dog",
  "cat sand dog"
]
Example 2:

Input:
s = "pineapplepenapple"
wordDict = ["apple", "pen", "applepen", "pine", "pineapple"]
Output:
[
  "pine apple pen apple",
  "pineapple pen apple",
  "pine applepen apple"
]
Explanation: Note that you are allowed to reuse a dictionary word.
 */

public class WordBreak2 {

    public static List<String> wordBreak(String s, List<String> wordDict) {
        Map<String, List<String>> wordCombinations = new HashMap<>();
        wordBreakUtil(s, wordDict, wordCombinations);
        System.out.println(wordCombinations);
        return wordCombinations.get(s);
    }

    public static void wordBreakUtil(String s, List<String> wordDict, Map<String, List<String>> wordCombinations) {
        if (wordCombinations.containsKey(s)) {
            return;
        }
        List<String> currentWordCombinations = new ArrayList<>();
        if (wordDict.contains(s)) {
            currentWordCombinations.add(s);
        }

        for (int i = 0; i < s.length(); i++) {
            String part1 = s.substring(0, i + 1);
            String part2 = s.substring(i + 1);
            if (part2.isEmpty()) {
                continue;
            }

            if (wordDict.contains(part1)) {
                wordBreakUtil(part2, wordDict, wordCombinations);
                //will get part2 combinations if present in map
                for (String str : wordCombinations.get(part2)) {
                    currentWordCombinations.add(part1 + " " + str);
                }
            }
        }
        wordCombinations.put(s, currentWordCombinations);
    }

    public static void main(String[] args) {
        String s = "catsanddog";
        List<String> wordDict = Arrays.asList("cat", "cats", "and", "sand", "dog");
        System.out.println(wordBreak(s, wordDict));
        String s2 = "pineapplepenapple";
        List<String> wordDict2 = Arrays.asList("apple", "pen", "applepen", "pine", "pineapple");
        System.out.println(wordBreak(s2, wordDict2));
        String s3 = "aaaaaaaaaa";
        List<String> wordDict3 = Arrays.asList("a", "aa", "aaa", "aaaa", "aaaaa", "aaaaaa", "aaaaaaa");
        System.out.println(wordBreak(s3, wordDict3));
    }
}
