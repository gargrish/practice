package InterviewProblems;

/*
Given a linked list, remove the n-th node from the end of list and return its head.

Example:

Given linked list: 1->2->3->4->5, and n = 2.

After removing the second node from the end, the linked list becomes 1->2->3->5.
 */

public class RemoveNthFromEnd {
    public class ListNode {
        int key;
        ListNode next;

        ListNode(int key) {
            this.key = key;
            this.next = null;
        }
    }

    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode node = head;
        int size = 0;

        while (node != null) {
            size++;
            node = node.next;
        }

        if (size == n) {
            return head.next;
        }

        int removeIndex = size - n;

        node = head;
        int count = 1;

        while (node != null) {
            if (count == removeIndex) {
                node.next = node.next.next;
                break;
            } else {
                count++;
                node = node.next;
            }
        }

        return head;
    }
}
