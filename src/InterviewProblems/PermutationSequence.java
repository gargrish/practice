package InterviewProblems;

import java.util.ArrayList;
import java.util.List;

/*
Example 1:

Input: n = 3, k = 3
Output: "213"
Example 2:

Input: n = 4, k = 9
Output: "2314"
 */
public class PermutationSequence {
    public static String getPermutation(int n, int k) {
        if (k > getPermutationsCount(n)) {
            return "";
        }
        List<Integer> numbers = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            numbers.add(i);
        }
        return permutationsUtil(numbers, n, "", k - 1);
    }

    public static String permutationsUtil(List<Integer> numbers, int n, String result, int k) {
        if (n == 0) {
            return result;
        }
        int numberToAddIndex = k / (getPermutationsCount(n) / n);
        int numberToAdd = numbers.get(numberToAddIndex);
        numbers.remove(numberToAddIndex);
        return permutationsUtil(numbers, n - 1, result + numberToAdd, (k % (getPermutationsCount(n) / n)));
    }

    public static int getPermutationsCount(int n) {
        if (n == 0 || n == 1) {
            return 1;
        }
        return n * getPermutationsCount(n - 1);
    }

    public static void main(String[] args) {
        System.out.println(getPermutation(4, 1));
        System.out.println(getPermutation(4, 7));
        System.out.println(getPermutation(4, 8));
        System.out.println(getPermutation(4, 9));
        System.out.println(getPermutation(4, 10));
        System.out.println(getPermutation(4, 16));
    }
}
