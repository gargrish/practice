package InterviewProblems;

import DataStructures.Trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

/*
Given a binary tree, return the zigzag level order traversal of its nodes' values.
(ie, from left to right, then right to left for the next level and alternate between).

For example:
Given binary tree [3,9,20,null,null,15,7],
    3
   / \
  9  20
    /  \
   15   7
return its zigzag level order traversal as:
[
  [3],
  [20,9],
  [15,7]
]
 */

public class BinaryZigzagOrderTraversal {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> order = new ArrayList<>();
        traversal(root, order, 0);
        List<List<Integer>> zigzagOrder = new ArrayList<>();
        for (int i = 0; i < order.size(); i++) {
            if (i % 2 == 0) {
                zigzagOrder.add(order.get(i));
            } else {
                zigzagOrder.add(reverseList(order.get(i)));
            }
        }
        return zigzagOrder;
    }

    public List<Integer> reverseList(List<Integer> list) {
        List<Integer> reverse = new ArrayList<>();
        for (int j = list.size() - 1; j >= 0; j--) {
            reverse.add(list.get(j));
        }
        return reverse;
    }

    public void traversal(TreeNode node, List<List<Integer>> order, int level) {
        if (node == null) {
            return;
        }
        if (order.size() <= level) {
            order.add(new ArrayList<>());
        }
        order.get(level).add(node.value);
        traversal(node.left, order, level + 1);
        traversal(node.right, order, level + 1);
    }
}
