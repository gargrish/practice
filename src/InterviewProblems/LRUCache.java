package InterviewProblems;

import java.util.HashMap;
import java.util.Map;

class LRUCache {

    public class ListNode {
        int key;
        ListNode next;
        ListNode prev;

        ListNode(int key) {
            this.key = key;
            this.next = null;
            this.prev = null;
        }
    }

    Map<Integer, ListNode> keyListNodeMap;
    public Map<Integer, Integer> cache;
    int size;
    ListNode head, end;

    LRUCache(int size) {
        cache = new HashMap<>();
        this.size = size;
        keyListNodeMap = new HashMap<>();
    }

    public int get(Integer key) {
        if (cache.containsKey(key)) {
            ListNode removeNode = keyListNodeMap.get(key);
            if (removeNode.prev != null) {
                removeNode.prev.next = removeNode.next;
            }
            if (removeNode.next != null) {
                removeNode.next.prev = removeNode.prev;
            }
            ListNode newHead = new ListNode(key);
            newHead.next = head;
            head.prev = newHead;
            head = newHead;
            keyListNodeMap.put(key, newHead);
            return cache.get(key);
        }
        return -1;
    }

    public void put(Integer key, Integer value) {
        if (!cache.containsKey(key) && cache.size() == size) {
            int removeKey = end.key;
            end = end.prev;
            end.next = null;
            cache.remove(removeKey);
            ListNode newNode = new ListNode(key);
            if (head == null) {
                head = newNode;
                end = newNode;
                keyListNodeMap.put(key, head);
            } else {
                newNode.next = head;
                head.prev = newNode;
                head = newNode;
                keyListNodeMap.put(key, newNode);
            }
        } else {
            if (head == null) {
                ListNode newNode = new ListNode(key);
                head = newNode;
                end = newNode;
                keyListNodeMap.put(key, newNode);
            } else {
                ListNode newHead;
                if (keyListNodeMap.containsKey(key)) {
                    newHead = keyListNodeMap.get(key);
                    newHead.prev.next = newHead.next;
                } else {
                    newHead = new ListNode(key);
                }
                head.prev = newHead;
                newHead.prev = null;
                newHead.next = head;
                head = newHead;
                keyListNodeMap.put(key, newHead);
            }
        }
        cache.put(key, value);
    }

    public static void main(String[] args) {
        LRUCache cache = new LRUCache(2);
        cache.put(1, 1);
        cache.put(2, 2);
        cache.get(1);       // returns 1
        cache.put(3, 3);    // evicts key 2
        cache.get(2);       // returns -1 (not found)
        cache.put(4, 4);    // evicts key 1
        cache.get(1);       // returns -1 (not found)
        cache.get(3);       // returns 3
        cache.get(4);       // returns 4
//        for (int i = 1; i < 12; i++) {
//            lruCache.put(i, i);
//        }
//        lruCache.get(5);
//        lruCache.get(8);
//        lruCache.get(2);
//        lruCache.get(0);
//        lruCache.get(3);
        while (cache.head != null) {
            System.out.println(cache.head.key);
            cache.head = cache.head.next;
        }
    }
}
