package InterviewProblems;

/*
Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.
 */
public class LongestPalindrome {
    public static String longestPalindrome(String s) {
        if (s.isBlank()) {
            return s;
        }
        int max = Integer.MIN_VALUE;
        int maxI = 0;
        int maxJ = 0;
        String output = "";
        boolean[][] isPalindrome = new boolean[s.length()][s.length()];
        for (int i = 0; i < s.length(); i++) {
            isPalindrome[i][i] = true;
        }

        int x = 1;
        while (x < s.length()) {
            for (int i = 0; i < s.length() - x; i++) {
                int j = i + x;
                if (x == 1 && s.charAt(i) == s.charAt(j)) {
                    isPalindrome[i][j] = true;
                    if (j - i > max) {
                        max = j - i;
                        maxI = i;
                        maxJ = j;
                    }
                } else if (isPalindrome[i + 1][j - 1] && s.charAt(i) == s.charAt(j)) {
                    isPalindrome[i][j] = true;
                    if (j - i > max) {
                        max = j - i;
                        maxI = i;
                        maxJ = j;
                    }
                }
            }
            x++;
        }
        return s.substring(maxI, maxJ + 1);
    }

    public static void main(String[] args) {
        System.out.println(longestPalindrome("klvxwqyzugrdoaccdafdfrvxiowkcuedfhoixzipxrkzbvpusslsgfjocvidnpsnkqdfnnzzawzsslwnvvjyoignsfbxkgrokzyusxikxumrxlzzrnbtrixxfioormoyyejashrowjqqzifacecvoruwkuessttlexvdptuvodoavsjaepvrfvbdhumtuvxufzzyowiswokioyjtzzmevttheeyjqcldllxvjraeyflthntsmipaoyjixygbtbvbnnrmlwwkeikhnnmlfspjgmcxwbjyhomfjdcnogqjviggklplpznfwjydkxzjkoskvqvnxfzdrsmooyciwulvtlmvnjbbmffureoilszlonibbcwfsjzguxqrjwypwrskhrttvnqoqisdfuifqnabzbvyzgbxfvmcomneykfmycevnrcsyqclamfxskmsxreptpxqxqidvjbuduktnwwoztvkuebfdigmjqfuolqzvjincchlmbrxpqgguwuyhrdtwqkdlqidlxzqktgzktihvlwsbysjeykiwokyqaskjjngovbagspyspeghutyoeahhgynzsyaszlirmlekpboywqdliumihwnsnwjc"));
//        System.out.println(longestPalindrome("aaaabbbaaaadw"));
//        System.out.println(longestPalindrome("ababababa"));
    }
}
