package InterviewProblems;

import java.util.ArrayList;
import java.util.List;

/*
Matrix Spiral
Input:
[
 [ 1, 2, 3 ],
 [ 4, 5, 6 ],
 [ 7, 8, 9 ]
]
Output: [1,2,3,6,9,8,7,4,5]
 */
public class MatrixSpiral {

    public static List<Integer> spiralOrder(int[][] matrix) {
        if (matrix.length == 0) {
            return new ArrayList<>();
        }
        List<Integer> output = new ArrayList<>();
        int size = matrix.length * matrix[0].length;
        int row;
        int col;
        int layer = (matrix.length + 1) / 2;
        for (int i = 0; i < layer; i++) {
            row = col = i;
            while (col < matrix[row].length - i && output.size() < size) {
                output.add(matrix[row][col]);
                col++;
            }
            col--;
            row++;
            while (row < matrix.length - i && output.size() < size) {
                output.add(matrix[row][col]);
                row++;
            }
            row--;
            col--;
            while (col >= i && output.size() < size) {
                output.add(matrix[row][col]);
                col--;
            }
            col++;
            row--;
            while (row > i && output.size() < size) {
                output.add(matrix[row][col]);
                row--;
            }
        }
        return output;
    }

    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16},
                {13, 14, 15, 16}
        };
        int[][] matrix2 = {
                {7},
                {9},
                {6},
        };
        System.out.println(spiralOrder(matrix));
        System.out.println(spiralOrder(matrix2));

    }
}
