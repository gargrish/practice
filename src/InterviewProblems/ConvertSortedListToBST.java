package InterviewProblems;

import DataStructures.Trees.TreeNode;

/*
Given a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.

For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.

Example:

Given the sorted linked list: [-10,-3,0,5,9],

One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:

      0
     / \
   -3   9
   /   /
 -10  5
 */

public class ConvertSortedListToBST {
    public class ListNode {
        int key;
        ListNode next;
        ListNode prev;

        ListNode(int key) {
            this.key = key;
            this.next = null;
            this.prev = null;
        }
    }

    private ListNode head;

    public TreeNode sortedListToBST(ListNode head) {
        int length = findLength(head);
        this.head = head;
        return convertListToBST(0, length - 1);
    }

    public TreeNode convertListToBST(int start, int end) {
        if (start > end) {
            return null;
        }

        int mid = (start + end) / 2;

        TreeNode left = convertListToBST(start, mid - 1);

        TreeNode treeHead = new TreeNode(head.key);

        treeHead.left = left;

        this.head = this.head.next;

        treeHead.right = convertListToBST(mid + 1, end);

        return treeHead;
    }

    public int findLength(ListNode node) {
        if (node == null) {
            return 0;
        }
        int length = 0;
        while (node != null) {
            node = node.next;
            length++;
        }
        return length;
    }
}
