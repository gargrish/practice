package InterviewProblems;

import java.util.ArrayList;
import java.util.List;

/*
A string S of lowercase letters is given. We want to partition this string into as many parts as possible so that each letter appears in at most one part, and return a list of integers representing the size of these parts.

Example 1:
Input: S = "ababcbacadefegdehijhklij"
Output: [9,7,8]
Explanation:
The partition is "ababcbaca", "defegde", "hijhklij".
This is a partition so that each letter appears in at most one part.
A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it splits S into less parts.
 */

public class PartitionLabels {

    public List<Integer> partitionLabels(String s) {
        List<Integer> partitionLabelsLengths = new ArrayList<>();
        int[] lastIndexes = new int[26];

        for (int i = 0; i < s.length(); i++) {
            lastIndexes[s.charAt(i) - 'a'] = i;
        }

        int j = 0, startIndex = 0;
        for (int i = 0; i < s.length(); i++) {
            j = Math.max(j, lastIndexes[s.charAt(i) - 'a']);
            if (j == i) {
                partitionLabelsLengths.add(j - startIndex + 1);
                startIndex = j + 1;
            }
        }
        return partitionLabelsLengths;
    }
}
