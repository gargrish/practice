package InterviewProblems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]
 */

public class TopKFrequent {
    public static int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> numberFrequency = new HashMap<>();
        for (int num : nums) {
            numberFrequency.put(num, numberFrequency.getOrDefault(num, 0) + 1);
        }
        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(numberFrequency.entrySet());
        divide(list, k, 0, list.size() - 1);
        int[] output = new int[k];
        int index = 0;
        for (int i = 0; i < list.size(); i++) {
            if (i != k - 1 && list.get(i).getValue() >= list.get(k - 1).getValue()) {
                output[index++] = list.get(i).getKey();
            }
        }
        output[index] = list.get(k - 1).getKey();
        return output;
    }


    public static void divide(List<Map.Entry<Integer, Integer>> list, int k, int start, int end) {
        if (start >= end) {
            return;
        }
        int partition = partition(list, start, end);
        if (partition == k - 1) {
            return;
        }
        if (partition > k - 1) {
            divide(list, k, start, partition - 1);
        } else {
            divide(list, k, partition + 1, end);
        }
    }

    public static int partition(List<Map.Entry<Integer, Integer>> list, int start, int end) {
        if (start >= end) {
            return start;
        }
        List<Map.Entry<Integer, Integer>> larger = new ArrayList<>();
        List<Map.Entry<Integer, Integer>> smaller = new ArrayList<>();
        Map.Entry<Integer, Integer> lastElement = list.get(end);
        for (int i = start; i < end; i++) {
            if (list.get(i).getValue() >= list.get(end).getValue()) {
                larger.add(list.get(i));
            } else {
                smaller.add(list.get(i));
            }
        }
        for (Map.Entry<Integer, Integer> large : larger) {
            list.remove(start);
            list.add(start++, large);
        }
        int pivot = start;
        list.remove(start);
        list.add(start++, lastElement);
        for (Map.Entry<Integer, Integer> small : smaller) {
            list.remove(start);
            list.add(start++, small);
        }
        return pivot;
    }

    public static void main(String[] args) {
        int[] nums = {2, 3, 4, 1, 4, 0, 4, -1, -2, -1};
        int k = 2;
        int[] res = topKFrequent(nums, k);
        for (int i : res) {
            System.out.println(i);
        }
    }
}
