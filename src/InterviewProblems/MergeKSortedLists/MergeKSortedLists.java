package InterviewProblems.MergeKSortedLists;

import java.util.*;

public class MergeKSortedLists {

    public static List<Integer> merge(List<Integer>[] lists) {
        List<Integer> output = new ArrayList<>();
        PriorityQueue<NumberWithListIndex> pQueue = new PriorityQueue<>((o1, o2) -> {
            if (o1.value < o2.value) {
                return -1;
            }
            return 1;
        });
        int[] indexes = new int[lists.length];
        for (int i = 0; i < lists.length; i++) {
            pQueue.add(new NumberWithListIndex(i, lists[i].get(0)));
            indexes[i]++;
        }
        while (!pQueue.isEmpty()) {
            NumberWithListIndex temp = pQueue.poll();
            output.add(temp.value);
            if (indexes[temp.index] < lists[temp.index].size()) {
                pQueue.add(new NumberWithListIndex(temp.index, lists[temp.index].get(indexes[temp.index])));
                indexes[temp.index]++;
            }
        }
        return output;
    }

    public static void main(String[] args) {
        List<Integer> l1 = Arrays.asList(1, 2, 3);
        List<Integer> l2 = Arrays.asList(1, 2, 8);
        List<Integer> l3 = Arrays.asList(3, 6, 9);
        List<Integer> l4 = Arrays.asList(1, 1);
        List<Integer>[] lists = new List[]{l1, l2, l3, l4};
        System.out.println(merge(lists));
    }
}
