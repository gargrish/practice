package InterviewProblems.MergeKSortedLists;

public class NumberWithListIndex {
    int index;
    int value;

    public NumberWithListIndex() {
    }

    public NumberWithListIndex(int index, int value) {
        this.index = index;
        this.value = value;
    }
}
