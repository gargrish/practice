package InterviewProblems;

import DataStructures.Trees.TreeNode;

/*
Given an integer array with no duplicates. A maximum tree building on this array is defined as follow:

The root is the maximum number in the array.
The left subtree is the maximum tree constructed from left part subarray divided by the maximum number.
The right subtree is the maximum tree constructed from right part subarray divided by the maximum number.
Construct the maximum tree by the given array and output the root node of this tree.

Example 1:
Input: [3,2,1,6,0,5]
Output: return the tree root node representing the following tree:

      6
    /   \
   3     5
    \    /
     2  0
       \
        1
 */

public class ContructMaxBinaryTree {
    public TreeNode constructMaximumBinaryTree(int[] nums) {
        TreeNode root = new TreeNode();
        return maxBinaryTree(nums, root, 0, nums.length);
    }

    public TreeNode maxBinaryTree(int[] nums, TreeNode root, int start, int end) {
        if (start >= end) {
            return null;
        }

        int max = nums[start];
        int maxIndex = start;

        for (int i = start + 1; i < end; i++) {
            if (nums[i] > max) {
                max = nums[i];
                maxIndex = i;
            }
        }

        if (root == null) {
            root = new TreeNode();
        }

        root.value = max;

        root.left = maxBinaryTree(nums, root.left, start, maxIndex);

        root.right = maxBinaryTree(nums, root.right, maxIndex + 1, end);

        return root;
    }
}
