package InterviewProblems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
Output: [[1,2],[3,10],[12,16]]
Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].
 */
public class InsertInterval {
    static class Interval {
        int start;
        int end;

        Interval() {
        }

        Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public static int[][] insert(int[][] intervals, int[] newInterval) {
        if (intervals.length == 0) {
            return new int[][]{newInterval};
        }
        List<Interval> outputList = new ArrayList<>();
        boolean addNewInterval = true;
        for (int i = 0; i < intervals.length; i++) {
            int[] temp = intersectAndMerge(intervals[i], newInterval);
            if (temp == null) {
                outputList.add(new Interval(intervals[i][0], intervals[i][1]));
                continue;
            }
            addNewInterval = false;
            if (outputList.size() > 0) {
                int[] temp2 = new int[]{outputList.get(outputList.size() - 1).start, outputList.get(outputList.size() - 1).end};
                int[] temp3 = intersectAndMerge(temp2, temp);
                if (temp3 == null) {
                    outputList.add(new Interval(temp[0], temp[1]));
                    continue;
                }
                outputList.remove(outputList.size() - 1);
                outputList.add(new Interval(temp3[0], temp3[1]));
            } else {
                outputList.add(new Interval(temp[0], temp[1]));
            }
        }
        if (addNewInterval) {
            outputList.add(new Interval(newInterval[0], newInterval[1]));
        }

        Collections.sort(outputList, (o1, o2) -> {
            if (o1.start < o2.start) {
                return -1;
            }
            if (o1.start > o2.start) {
                return 1;
            }
            return 0;
        });

        int[][] output = new int[outputList.size()][2];
        int index = 0;
        for (Interval i : outputList) {
            output[index++] = new int[]{i.start, i.end};
        }
        return output;
    }

    public static int[] intersectAndMerge(int[] i1, int[] i2) {
        if (i1[0] >= i2[0] && i1[0] <= i2[1] || i1[1] >= i2[0] && i1[1] <= i2[1] ||
                i2[0] >= i1[0] && i2[0] <= i1[1] || i2[1] >= i1[0] && i2[1] <= i1[1]) {
            return new int[]{Math.min(i1[0], i2[0]), Math.max(i1[1], i2[1])};
        }
        return null;
    }

    public static void main(String[] args) {
        int[][] intervals = {{1, 2}, {3, 5}, {6, 7}, {8, 10}, {12, 16}};
        int[][] resp = insert(intervals, new int[]{4, 8});
        for (int i = 0; i < resp.length; i++)
            System.out.println(resp[i][0] + " " + resp[i][1]);
    }
}
