package InterviewProblems;

import DataStructures.Trees.TreeNode;

import java.util.*;

public class BinaryTreeVerticalOrderTraversal {
    public class Pair<T, U> {
        private final T key;
        private final U value;

        public Pair(T key, U value) {
            this.key = key;
            this.value = value;
        }

        public T getKey() {
            return this.key;
        }

        public U getValue() {
            return this.value;
        }
    }

    private int minColumn = 0, maxColumn = 0;

    public List<List<Integer>> verticalOrder(TreeNode root) {
        List<List<Integer>> verticalOrderedList = new ArrayList<>();

        if (root == null) {
            return verticalOrderedList;
        }

        Map<Integer, List<Pair<Integer, Integer>>> columnTable = new HashMap<>();

        traverse(root, columnTable, 0, 0);

        for (int i = minColumn; i <= maxColumn; i++) {
            Collections.sort(columnTable.get(i), (o1, o2) -> {
                if (o1.getKey() < o2.getKey()) {
                    return -1;
                }
                if (o1.getKey() > o2.getKey()) {
                    return 1;
                }
                return 0;
            });

            List<Integer> sortedRows = new ArrayList<>();

            for (Pair<Integer, Integer> p : columnTable.get(i)) {
                sortedRows.add(p.getValue());
            }
            verticalOrderedList.add(sortedRows);
        }
        return verticalOrderedList;
    }

    public void traverse(TreeNode root, Map<Integer, List<Pair<Integer, Integer>>> columnTable, int column, int row) {
        if (root == null) {
            return;
        }

        if (!columnTable.containsKey(column)) {
            columnTable.put(column, new ArrayList<>());
        }

        columnTable.get(column).add(new Pair<>(row, root.value));
        minColumn = Math.min(minColumn, column);
        maxColumn = Math.max(maxColumn, column);
        traverse(root.left, columnTable, column - 1, row + 1);
        traverse(root.right, columnTable, column + 1, row + 1);
    }
}
