package InterviewProblems.ParallelDownloads;

import java.util.Comparator;

public class Download {
    int startTime;
    int endTime;

    public Download() {
    }

    public Download(int startTime, int endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Download(Download download) {
        this.startTime = download.startTime;
        this.endTime = download.endTime;
    }

    @Override
    public String toString() {
        return this.startTime + " " + this.endTime;
    }
}
