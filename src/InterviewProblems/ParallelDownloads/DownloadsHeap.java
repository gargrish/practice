package InterviewProblems.ParallelDownloads;

public class DownloadsHeap {
    int index;
    Download download;

    DownloadsHeap() {
    }

    public DownloadsHeap(int index, Download download) {
        this.index = index;
        this.download = new Download(download);
    }
}
