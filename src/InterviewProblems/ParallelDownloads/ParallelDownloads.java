package InterviewProblems.ParallelDownloads;

import DataStructures.Heap.HeapObject;

import java.util.*;

public class ParallelDownloads {
    public static int parallel(List<Download> downloads) {
        List<Download> startTimeSorted = new LinkedList<>(downloads);
        List<Download> endTimeSorted = new LinkedList<>(downloads);
        Collections.sort(startTimeSorted, (o1, o2) -> {
            if (o1.startTime > o2.startTime) {
                return 1;
            }
            if (o1.startTime < o2.startTime) {
                return -1;
            }
            return 0;
        });
        Collections.sort(endTimeSorted, (o1, o2) -> {
            if (o1.endTime > o2.endTime) {
                return 1;
            }
            if (o1.endTime < o2.endTime) {
                return -1;
            }
            return 0;
        });
        int max = Integer.MIN_VALUE;
        int i = 0, j = 0, count = 0;
        while (i < startTimeSorted.size() && j < endTimeSorted.size()) {
            if (startTimeSorted.get(i).startTime < endTimeSorted.get(j).endTime) {
                count++;
                i++;
                max = Math.max(max, count);
            } else {
                count--;
                j++;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        List<Download> downloads = new ArrayList<>();
        downloads.add(new Download(1, 7));
        downloads.add(new Download(3, 9));
        downloads.add(new Download(4, 5));
        downloads.add(new Download(2, 12));
        downloads.add(new Download(10, 17));
        downloads.add(new Download(13, 19));
        downloads.add(new Download(14, 15));
        downloads.add(new Download(12, 22));
        downloads.add(new Download(11, 15));
        downloads.add(new Download(12, 14));
        downloads.add(new Download(12, 14));
        downloads.add(new Download(12, 14));
        downloads.add(new Download(12, 14));
        downloads.add(new Download(12, 14));
        downloads.add(new Download(1, 7));
        downloads.add(new Download(3, 9));
        downloads.add(new Download(3, 9));
        downloads.add(new Download(3, 9));
        downloads.add(new Download(3, 9));
        downloads.add(new Download(3, 9));
        downloads.add(new Download(3, 9));

        System.out.println(parallel(downloads));
    }
}
