package InterviewProblems;

import java.util.Arrays;

/*
Given a non-empty array of numbers, a0, a1, a2, … , an-1, where 0 ≤ ai < 231.

Find the maximum result of ai XOR aj, where 0 ≤ i, j < n.

Could you do this in O(n) runtime?

Example:

Input: [3, 10, 5, 25, 2, 8]

Output: 28

Explanation: The maximum result is 5 ^ 25 = 28.
 */

public class MaxXOR {
    public int findMaximumXOR(int[] nums) {
        BinaryTrie head = new BinaryTrie();
        initialize(nums, head);
        int[] maxXorNumbers = new int[nums.length];
        Arrays.fill(maxXorNumbers, Integer.MIN_VALUE);
        for (int i = 0; i < nums.length; i++) {
            String binaryNumber = Integer.toBinaryString(nums[i]);
            String binary = ("00000000000000000000000000000000" + binaryNumber).substring(binaryNumber.length());
            char[] binaryChars = binary.toCharArray();
            int binaryCharIndex = 0;
            BinaryTrie node = head;
            while (node.children[0] != null || node.children[1] != null) {
                if (binaryCharIndex == binaryChars.length) {
                    if (node.children[1] != null) {
                        node = node.children[1];
                    } else {
                        node = node.children[0];
                    }
                } else if (binaryChars[binaryCharIndex] == '1' && node.children[0] != null) {
                    node = node.children[0];
                    binaryCharIndex++;
                } else if (binaryChars[binaryCharIndex] == '0' && node.children[1] != null) {
                    node = node.children[1];
                    binaryCharIndex++;
                } else {
                    if (node.children[0] != null && node.children[1] == null) {
                        node = node.children[0];
                        binaryCharIndex++;
                    } else {
                        node = node.children[1];
                        binaryCharIndex++;
                    }
                }
                if (node.number >= 0) {
                    maxXorNumbers[i] = node.number;
                }
            }
        }
        int maxXor = Integer.MIN_VALUE;
        ;
        for (int i = 0; i < maxXorNumbers.length; i++) {
            maxXor = Math.max(maxXor, nums[i] ^ maxXorNumbers[i]);
        }
        return maxXor;
    }


    class BinaryTrie {
        char value;
        int number;
        BinaryTrie[] children;

        BinaryTrie() {
            this.children = new BinaryTrie[2];
        }

        BinaryTrie(char value) {
            this.value = value;
            this.children = new BinaryTrie[2];
        }

        @Override
        public String toString() {
            return this.value + " Number:" + this.number;
        }
    }

    public void initialize(int[] nums, BinaryTrie head) {
        BinaryTrie top = head;
        for (int i = 0; i < nums.length; i++) {
            head = top;
            String binaryNumber = Integer.toBinaryString(nums[i]);
            String binary = ("00000000000000000000000000000000" + binaryNumber).substring(binaryNumber.length());
            char[] binaryChars = binary.toCharArray();
            for (int j = 0; j < binaryChars.length; j++) {
                if (binaryChars[j] == '0') {
                    if (head.children[0] == null) {
                        head.children[0] = new BinaryTrie('0');
                    }
                    head = head.children[0];
                } else {
                    if (head.children[1] == null) {
                        head.children[1] = new BinaryTrie('1');
                    }
                    head = head.children[1];
                }
            }
            head.number = nums[i];
        }
    }
}
