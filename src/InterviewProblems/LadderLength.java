package InterviewProblems;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/*
Given two words (beginWord and endWord), and a dictionary's word list, find the length of shortest transformation sequence from beginWord to endWord, such that:

Only one letter can be changed at a time.
Each transformed word must exist in the word list.
Note:

Return 0 if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
"hot"
"dog"
["hot","cog","dog","tot","hog","hop","pot","dot"]
3
 */
public class LadderLength {
    public static class WordShortestDistance {
        String word;
        int distance;

        WordShortestDistance(String word, int distance) {
            this.word = word;
            this.distance = distance;
        }

        @Override
        public String toString() {
            return this.word + " " + distance;
        }
    }

    public static int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Map<String, Set<String>> nextWords = new HashMap<>();
        Set<String> wordListSet = ConcurrentHashMap.newKeySet();
        for (String word : wordList) {
            wordListSet.add(word);
        }
        formNextTransformations(beginWord, wordListSet, nextWords);
        if (!nextWords.containsKey(beginWord)) {
            return 0;
        }
        PriorityQueue<WordShortestDistance> wordTransformation = new PriorityQueue<>((o1, o2) -> {
            if (o1.distance < o2.distance) return -1;
            if (o1.distance > o2.distance) return 1;
            return 0;
        });

        Map<String, Integer> wordShortestDistanceMap = new HashMap<>();
        for (String word : nextWords.keySet()) {
            if (word.equals(beginWord)) {
                wordTransformation.add(new WordShortestDistance(word, 0));
                wordShortestDistanceMap.put(word, 0);
            } else if (nextWords.get(beginWord).contains(word)) {
                wordTransformation.add(new WordShortestDistance(word, 1));
                wordShortestDistanceMap.put(word, 1);
            } else {
                wordTransformation.add(new WordShortestDistance(word, Integer.MAX_VALUE));
                wordShortestDistanceMap.put(word, Integer.MAX_VALUE);
            }
        }

        while (wordTransformation.size() > 0) {
            WordShortestDistance currentWordDistance = wordTransformation.poll();
            if (currentWordDistance.distance == 0) {
                continue;
            }
            for (String str : nextWords.get(currentWordDistance.word)) {
                if (wordShortestDistanceMap.get(currentWordDistance.word) + 1 < wordShortestDistanceMap.get(str)) {
                    wordShortestDistanceMap.put(str, wordShortestDistanceMap.get(currentWordDistance.word) + 1);
                    wordTransformation.add(new WordShortestDistance(str, wordShortestDistanceMap.get(str)));
                }
            }
        }
        if (wordShortestDistanceMap.containsKey(endWord) && wordShortestDistanceMap.get(endWord) > 0) {
            return wordShortestDistanceMap.get(endWord) + 1;
        }
        return 0;
    }

    public static void formNextTransformations(String beginWord, Set<String> wordList, Map<String, Set<String>> nextWords) {
        for (String word : wordList) {
            findNextWords(beginWord, word, nextWords);
            wordList.remove(word);
            for (String word2 : wordList) {
                findNextWords(word, word2, nextWords);
            }
        }
    }

    public static void findNextWords(String word1, String word2, Map<String, Set<String>> nextWords) {
        int diff = 0;
        char[] word1Chars = word1.toCharArray();
        char[] word2Chars = word2.toCharArray();
        for (int i = 0; i < word1Chars.length; i++) {
            if (word1Chars[i] != word2Chars[i]) {
                diff++;
            }
        }
        if (diff == 1) {
            if (!nextWords.containsKey(word1)) {
                nextWords.put(word1, ConcurrentHashMap.newKeySet());
            }
            nextWords.get(word1).add(word2);
            if (!nextWords.containsKey(word2)) {
                nextWords.put(word2, ConcurrentHashMap.newKeySet());
            }
            nextWords.get(word2).add(word1);
        }
    }

    public static void main(String[] args) {
        String beginWord = "hit";
        String endWord = "cog";
        List<String> wordList = Arrays.asList("hot", "cog", "dot", "dog", "hit", "lot", "log");
        System.out.println(ladderLength(beginWord, endWord, wordList)); //this is coming correct
        String beginWord2 = "hot";
        String endWord2 = "dog";
        List<String> wordList2 = Arrays.asList("hot", "cog", "dog", "tot", "hog", "hop", "pot", "dot");
        System.out.println(ladderLength(beginWord2, endWord2, wordList2)); //this should be 3
        String beginWord3 = "qa";
        String endWord3 = "sq";
        List<String> wordList3 = Arrays.asList("si", "go", "se", "cm", "so", "ph", "mt", "db", "mb", "sb", "kr", "ln", "tm", "le", "av", "sm", "ar", "ci", "ca", "br", "ti", "ba", "to", "ra", "fa", "yo", "ow", "sn", "ya", "cr", "po", "fe", "ho", "ma", "re", "or", "rn", "au", "ur", "rh", "sr", "tc", "lt", "lo", "as", "fr", "nb", "yb", "if", "pb", "ge", "th", "pm", "rb", "sh", "co", "ga", "li", "ha", "hz", "no", "bi", "di", "hi", "qa", "pi", "os", "uh", "wm", "an", "me", "mo", "na", "la", "st", "er", "sc", "ne", "mn", "mi", "am", "ex", "pt", "io", "be", "fm", "ta", "tb", "ni", "mr", "pa", "he", "lr", "sq", "ye");
        System.out.println(ladderLength(beginWord3, endWord3, wordList3));// this should be 5
    }
}
