package InterviewProblems;

import java.util.Stack;

/*
Given two binary strings, return their sum (also a binary string).

The input strings are both non-empty and contains only characters 1 or 0.

Example 1:

Input: a = "11", b = "1"
Output: "100"
Example 2:

Input: a = "1010", b = "1011"
Output: "10101"
 */

public class AddBinary {
    public String addBinary(String a, String b) {
        Stack<Character> stack1 = new Stack<>();
        Stack<Character> stack2 = new Stack<>();
        for (int i = 0; i < a.length(); i++) {
            stack1.add(a.charAt(i));
        }
        for (int i = 0; i < b.length(); i++) {
            stack2.add(b.charAt(i));
        }
        Stack<Character> output = new Stack<>();
        char sum = '0', carry = '0';
        char num1, num2;
        while (!stack1.isEmpty() || !stack2.isEmpty()) {
            num1 = '0';
            num2 = '0';
            if (!stack1.isEmpty()) {
                num1 = stack1.pop();
            }
            if (!stack2.isEmpty()) {
                num2 = stack2.pop();
            }
            if (carry == '1') {
                if (num1 == num2) {
                    output.add('1');
                    if (num1 == '1') {
                        carry = '1';
                    } else {
                        carry = '0';
                    }
                } else {
                    output.add('0');
                    carry = '1';
                }
            } else {
                if (num1 == num2) {
                    if (num1 == '1') {
                        carry = '1';
                    }
                    output.add('0');
                } else {
                    output.add('1');
                }
            }
        }
        if (carry == '1') {
            output.add('1');
        }
        String res = "";
        while (!output.isEmpty()) {
            res += output.pop();
        }
        return res;
    }
}
