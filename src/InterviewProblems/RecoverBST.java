package InterviewProblems;

import java.util.ArrayList;
import java.util.List;

/*
Two elements of a binary search tree (BST) are swapped by mistake.

Recover the tree without changing its structure.

Example 1:

Input: [1,3,null,null,2]

   1
  /
 3
  \
   2

Output: [3,1,null,null,2]

   3
  /
 1
  \
   2
 */

public class RecoverBST {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public static void recoverTree(TreeNode root) {
        List<Integer> inorderTreeNumbers = new ArrayList<>();
        inorder(root, inorderTreeNumbers);
        int[] swapNumbers = exchange(inorderTreeNumbers);
        recover(root, 2, swapNumbers[0], swapNumbers[1]);
    }

    public static int[] exchange(List<Integer> inorderTreeNumbers) {
        int x = -1, y = -1;
        for (int i = 0; i < inorderTreeNumbers.size() - 1; i++) {
            if (inorderTreeNumbers.get(i + 1) < inorderTreeNumbers.get(i)) {
                y = inorderTreeNumbers.get(i + 1);
                if (x == -1) {
                    x = inorderTreeNumbers.get(i);
                } else {
                    break;
                }
            }
        }
        return new int[]{x, y};
    }

    public static void recover(TreeNode root, int count, int x, int y) {
        if (root == null) {
            return;
        }
        if (root.val == x || root.val == y) {
            root.val = root.val == x ? y : x;
            count--;
            if (count == 0) {
                return;
            }
        }
        recover(root.left, count, x, y);
        recover(root.right, count, x, y);
    }

    public static void inorder(TreeNode node, List<Integer> inorderTreeNumbers) {
        if (node == null) {
            return;
        }
        inorder(node.left, inorderTreeNumbers);
        inorderTreeNumbers.add(node.val);
        inorder(node.right, inorderTreeNumbers);
    }
}
