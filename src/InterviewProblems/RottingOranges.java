package InterviewProblems;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/*
Example 1:

Input: [[2,1,1],[1,1,0],[0,1,1]]
Output: 4


Example 2:
Input: [[2,1,1],[0,1,1],[1,0,1]]
Output: -1
Explanation:  The orange in the bottom left corner (row 2, column 0) is never rotten, because rotting only happens 4-directionally.

Example 3:
Input: [[0,2]]
Output: 0
Explanation:  Since there are already no fresh oranges at minute 0, the answer is just 0.
 */

public class RottingOranges {
    public class Position {
        int row;
        int col;

        Position(int row, int col) {
            this.row = row;
            this.col = col;
        }

        @Override
        public String toString() {
            return this.row + " " + this.col;
        }
    }

    public int orangesRotting(int[][] grid) {
        int totalFreshOranges = 0;
        int time = 0;
        Set<Position> toBeProcessedRottenOrangesPosition = ConcurrentHashMap.newKeySet();
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                //rotten orange
                if (grid[i][j] == 2) {
                    toBeProcessedRottenOrangesPosition.add(new Position(i, j));
                } else if (grid[i][j] == 1) {
                    totalFreshOranges++;
                }
            }
        }
        if (totalFreshOranges == 0) {
            return 0;
        }
        while (totalFreshOranges > 0 && toBeProcessedRottenOrangesPosition.size() > 0) {
            Set<Position> temp = new HashSet<>();
            for (Position pos : toBeProcessedRottenOrangesPosition) {
                totalFreshOranges -= process(grid, pos.row, pos.col, temp);
            }
            toBeProcessedRottenOrangesPosition = temp;
            time++;
        }

        if (totalFreshOranges == 0) {
            return time;
        }
        return -1;
    }

    public int process(int[][] grid, int i, int j, Set<Position> positions) {
        int count = 0;

        if (i >= 0 && i < grid.length && (j + 1) >= 0 && (j + 1) < grid[0].length && grid[i][j + 1] == 1) {
            grid[i][j + 1] = 2;
            positions.add(new Position(i, j + 1));
            count++;
        }
        if (i >= 0 && i < grid.length && (j - 1) >= 0 && (j - 1) < grid[0].length && grid[i][j - 1] == 1) {
            grid[i][j - 1] = 2;
            positions.add(new Position(i, j - 1));
            count++;
        }
        if ((i + 1) >= 0 && (i + 1) < grid.length && j >= 0 && j < grid[0].length && grid[i + 1][j] == 1) {
            grid[i + 1][j] = 2;
            positions.add(new Position(i + 1, j));
            count++;
        }
        if ((i - 1) >= 0 && (i - 1) < grid.length && j >= 0 && j < grid[0].length && grid[i - 1][j] == 1) {
            grid[i - 1][j] = 2;
            positions.add(new Position(i - 1, j));
            count++;
        }
        return count;
    }

    public static void main(String[] args) {
        RottingOranges object = new RottingOranges();
        int[][] grid = {
                {2, 1, 1},
                {1, 1, 0},
                {0, 1, 1},
        };
        System.out.println(object.orangesRotting(grid));
        System.out.println();
        System.out.println();
        System.out.println();

        int[][] grid2 = {
                {2, 1, 1},
                {0, 1, 1},
                {1, 0, 1},
        };
        System.out.println(object.orangesRotting(grid2));
        System.out.println();
        System.out.println();
        System.out.println();

        int[][] grid3 = {
                {2, 2, 2, 1, 1},
        };
        System.out.println(object.orangesRotting(grid3));
        System.out.println();
        System.out.println();
        System.out.println();

        int[][] grid4 = {
                {1},
                {2},
                {1},
                {1},
                {1},
        };
        System.out.println(object.orangesRotting(grid4));
    }
}
