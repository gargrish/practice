package InterviewProblems;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

Count the number of distinct islands. An island is considered to be the same as another if and only if one island can be translated (and not rotated or reflected) to equal the other.

Example 1:
11000
11000
00011
00011
Given the above grid map, return 1.
Example 2:
11011
10000
00001
11011
Given the above grid map, return 3.

Notice that:
11
1
and
 1
11
are considered different island shapes, because we do not consider reflection / rotation.
 */

public class NumberOfDistinctIslands {
    public int numDistinctIslands(int[][] grid) {
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        Set<List<Integer>> shapes = new HashSet<>();

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1 && !visited[i][j]) {
                    List<Integer> shape = new ArrayList<>();
                    explorePath(grid, i, j, 0, shape, visited);
                    if (shape.size() > 0) {
                        shapes.add(shape);
                    }
                }
            }
        }

        return shapes.size();
    }

    public void explorePath(int[][] grid, int row, int col, int path, List<Integer> shape, boolean[][] visited) {
        if (row >= 0 && row < grid.length && col >= 0 && col < grid[0].length &&
                grid[row][col] == 1 && !visited[row][col]) {
            visited[row][col] = true;
            shape.add(path);

            explorePath(grid, row + 1, col, 1, shape, visited);
            explorePath(grid, row - 1, col, 2, shape, visited);
            explorePath(grid, row, col + 1, 3, shape, visited);
            explorePath(grid, row, col - 1, 4, shape, visited);

            shape.add(0);
        }
    }
}
