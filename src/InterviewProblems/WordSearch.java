package InterviewProblems;

/*
Example:

board =
[
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
]

Given word = "ABCCED", return true.
Given word = "SEE", return true.
Given word = "ABCB", return false.
 */

public class WordSearch {
    public static boolean exist(char[][] board, String word) {
        boolean[][] hasBeenVisited = new boolean[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == word.toCharArray()[0]) {
                    boolean temp = wordUtil(board, i, j, word, word.length(), hasBeenVisited);
                    if (temp) {
                        return true;
                    }
                    hasBeenVisited = new boolean[board.length][board[0].length];
                }
            }
        }
        return false;
    }

    public static boolean wordUtil(char[][] board, int i, int j, String word, int wordIndex, boolean[][] hasBeenVisited) {
        if (word.isBlank()) {
            return true;
        }
        if (i < 0 || i >= board.length || j < 0 || j >= board[0].length) {
            return false;
        }
        if (hasBeenVisited[i][j]) {
            return false;
        }
        if (board[i][j] == word.toCharArray()[0]) {
            hasBeenVisited[i][j] = true;
            boolean[][] temp = new boolean[hasBeenVisited.length][hasBeenVisited[0].length];
            for (int x = 0; x < hasBeenVisited.length; x++) {
                for (int y = 0; y < hasBeenVisited[x].length; y++) {
                    temp[x][y] = hasBeenVisited[x][y];
                }
            }
            word = word.substring(1);
            if (word.isBlank()) {
                return true;
            }
            if (wordUtil(board, i, j + 1, word, wordIndex + 1, temp)) {
                return true;
            }
            temp = new boolean[hasBeenVisited.length][hasBeenVisited[0].length];
            for (int x = 0; x < hasBeenVisited.length; x++) {
                for (int y = 0; y < hasBeenVisited[x].length; y++) {
                    temp[x][y] = hasBeenVisited[x][y];
                }
            }
            if (wordUtil(board, i + 1, j, word, wordIndex + 1, temp)) {
                return true;
            }
            temp = new boolean[hasBeenVisited.length][hasBeenVisited[0].length];
            for (int x = 0; x < hasBeenVisited.length; x++) {
                for (int y = 0; y < hasBeenVisited[x].length; y++) {
                    temp[x][y] = hasBeenVisited[x][y];
                }
            }
            if (wordUtil(board, i - 1, j, word, wordIndex + 1, temp)) {
                return true;
            }
            temp = new boolean[hasBeenVisited.length][hasBeenVisited[0].length];
            for (int x = 0; x < hasBeenVisited.length; x++) {
                for (int y = 0; y < hasBeenVisited[x].length; y++) {
                    temp[x][y] = hasBeenVisited[x][y];
                }
            }
            if (wordUtil(board, i, j - 1, word, wordIndex + 1, temp)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        char[][] board = {
                {'A', 'B', 'C', 'E'},
                {'S', 'F', 'C', 'S'},
                {'A', 'D', 'E', 'E'}
        };
        char[][] board2 = {
                {'A', 'B'},
                {'C', 'D'}
        };
        char[][] board3 = {
                {'C', 'A', 'A'},
                {'A', 'A', 'A'},
                {'B', 'C', 'D'}
        };
        char[][] board4 = {
                {'A', 'B', 'C', 'E'},
                {'S', 'F', 'E', 'S'},
                {'A', 'D', 'E', 'E'}
        };
        System.out.println(exist(board, "ABCCED"));
        System.out.println(exist(board, "ABCB"));
        System.out.println(exist(board2, "ABDC"));
        System.out.println(exist(board2, "ABCD"));
        System.out.println(exist(board2, "ACDB"));
        System.out.println(exist(board3, "AAB"));
        System.out.println(exist(board4, "ABCEFSADEESE"));
    }
}
