package InterviewProblems;

public class SuperPower {
    public static int superPow(int a, int[] b) {
        int n=b[b.length-1];
        int index=1;
        for(int i=b.length-2;i>=0;i--) {
            n = n + 10*index*b[i];
            index++;
        }
        int power = pow(a%1337, n)%1337;
        return power;
    }

    public static int pow(int x, int n) {
        if(n == 0) {
            return 1;
        }
        int half = pow(x%1337, n/2)%1337;
        half = half%1337;
        if(n % 2 == 0) {
            int temp = half * half;
            return temp%1337;
        }
        int temp = x * half * half;
        return temp%1337;
    }

    public static void main(String[] args) {

        System.out.println(superPow(2147483647, new int[]{2,0,0}));
    }
}
