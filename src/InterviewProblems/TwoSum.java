package InterviewProblems;

import java.util.*;

/*
Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
 */
public class TwoSum {
    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> numbersWithIndex = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            numbersWithIndex.put(i, nums[i]);
        }
        List<Map.Entry<Integer, Integer>> numbersWithIndexList = new ArrayList<>(numbersWithIndex.entrySet());
        Collections.sort(numbersWithIndexList, (o1, o2) -> {
            if (o1.getValue() < o2.getValue()) {
                return -1;
            }
            if (o1.getValue() > o2.getValue()) {
                return 1;
            }
            return 0;
        });
        for (int i = 0, j = numbersWithIndexList.size() - 1; i < j; ) {
            if (numbersWithIndexList.get(i).getValue() + numbersWithIndexList.get(j).getValue() == target) {
                return new int[]{numbersWithIndexList.get(i).getKey(), numbersWithIndexList.get(j).getKey()};
            }
            if (numbersWithIndexList.get(i).getValue() + numbersWithIndexList.get(j).getValue() > target) {
                j--;
            } else {
                i++;
            }
        }
        return new int[]{};
    }

    public static void main(String[] args) {
        int[] sum = twoSum(new int[]{2, 7, 11, 15}, 9);
        System.out.println(sum[0] + " " + sum[1]);
    }
}
