package InterviewProblems;

/*
You are given two non-empty linked lists representing two non-negative integers.
The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example:

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.
 */

public class AddTwoNumbers {
    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode output = new ListNode();
        ListNode head = output;
        int num1, num2;
        int carry = 0;
        while (l1 != null || l2 != null) {
            num1 = 0;
            num2 = 0;
            if (l1 != null) {
                num1 = l1.val;
                l1 = l1.next;
            }
            if (l2 != null) {
                num2 = l2.val;
                l2 = l2.next;
            }
            int sum = num1 + num2 + carry;

            if (sum < 10) {
                output.next = new ListNode(sum);
                carry = 0;
            } else {
                output.next = new ListNode(sum % 10);
                carry = sum / 10;
            }
            output = output.next;
        }
        if (carry > 0) {
            output.next = new ListNode(carry);
        }
        return head.next;
    }
}
