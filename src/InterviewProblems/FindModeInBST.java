package InterviewProblems;

import DataStructures.Trees.TreeNode;

import java.util.*;

/*
Given a binary search tree (BST) with duplicates, find all the mode(s) (the most frequently occurred element) in the given BST.

Assume a BST is defined as follows:

The left subtree of a node contains only nodes with keys less than or equal to the node's key.
The right subtree of a node contains only nodes with keys greater than or equal to the node's key.
Both the left and right subtrees must also be binary search trees.


For example:
Given BST [1,null,2,2],

   1
    \
     2
    /
   2


return [2].

Note: If a tree has more than one mode, you can return them in any order.

Follow up: Could you do that without using any extra space? (Assume that the implicit stack space incurred due to recursion does not count).
 */

public class FindModeInBST {
    class ModeFrequency {
        int mode;
        int frequency;

        ModeFrequency(int mode, int frequency) {
            this.mode = mode;
            this.frequency = frequency;
        }
    }

    public int[] findMode(TreeNode root) {
        if (root == null) {
            return new int[]{};
        }
        Map<Integer, Integer> modeFrequency = new HashMap<>();
        inorder(root, modeFrequency);
        PriorityQueue<ModeFrequency> modeFrequencyMaxHeap = new PriorityQueue<>((o1, o2) -> {
            if (o1.frequency > o2.frequency) {
                return -1;
            }
            if (o1.frequency < o2.frequency) {
                return 1;
            }
            return 0;
        });
        for (int mode : modeFrequency.keySet()) {
            modeFrequencyMaxHeap.add(new ModeFrequency(mode, modeFrequency.get(mode)));
        }
        List<Integer> modesList = new ArrayList<>();
        ModeFrequency maxFrequencyMode = modeFrequencyMaxHeap.poll();
        modesList.add(maxFrequencyMode.mode);
        while (!modeFrequencyMaxHeap.isEmpty() && modeFrequencyMaxHeap.peek().frequency == maxFrequencyMode.frequency) {
            ModeFrequency temp = modeFrequencyMaxHeap.poll();
            modesList.add(temp.mode);
        }
        int[] modes = new int[modesList.size()];
        int index = 0;
        for (int mode : modesList) {
            modes[index++] = mode;
        }
        return modes;
    }

    public void inorder(TreeNode node, Map<Integer, Integer> modeFrequency) {
        if (node == null) {
            return;
        }
        inorder(node.left, modeFrequency);
        modeFrequency.put(node.value, modeFrequency.getOrDefault(node.value, 0) + 1);
        inorder(node.right, modeFrequency);
    }
}
