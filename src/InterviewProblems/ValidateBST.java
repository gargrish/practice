package InterviewProblems;

import DataStructures.Trees.TreeNode;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.Optional;

/*
Given a binary tree, determine if it is a valid binary search tree (BST).

Assume a BST is defined as follows:

The left subtree of a node contains only nodes with keys less than the node's key.
The right subtree of a node contains only nodes with keys greater than the node's key.
Both the left and right subtrees must also be binary search trees.


Example 1:

    2
   / \
  1   3

Input: [2,1,3]
Output: true
Example 2:

    5
   / \
  1   4
     / \
    3   6

Input: [5,1,4,null,null,3,6]
Output: false
Explanation: The root node's value is 5 but its right child's value is 4.
 */

public class ValidateBST {
    Integer max = null;
    public boolean isValidBST(TreeNode root) {
        if (root == null) {
            return true;
        }
        boolean isLeftSubtreeValid = isValidBST(root.left);
        if (max == null || max < root.value) {
            max = root.value;
        } else {
            return false;
        }
        boolean isRightSubtreeValid = isValidBST(root.right);
        return isLeftSubtreeValid && isRightSubtreeValid;
    }
}
