package InterviewProblems;

/*
Given a linked list, reverse the nodes of a linked list k at a time and return its modified list.

k is a positive integer and is less than or equal to the length of the linked list. If the number of nodes is not a multiple of k then left-out nodes in the end should remain as it is.

Example:

Given this linked list: 1->2->3->4->5

For k = 2, you should return: 2->1->4->3->5

For k = 3, you should return: 3->2->1->4->5
 */

public class ReverseListKGroups {
    public class ListNode {
        int key;
        ListNode next;
        ListNode prev;

        ListNode(int key) {
            this.key = key;
            this.next = null;
            this.prev = null;
        }
    }

    public ListNode reverseKGroup(ListNode head, int k) {
        int count = 0;
        ListNode start = head;
        while (count < k && start != null) {
            start = start.next;
            count++;
        }

        if (count == k) {
            ListNode reversedHead = reverseList(head, k);
            head.next = reverseKGroup(start, k);
            return reversedHead;
        }
        return head;
    }

    public ListNode reverseList(ListNode head, int k) {
        ListNode newHead = null;
        ListNode start = head;
        while (k > 0) {
            ListNode nextNode = start.next;

            start.next = newHead;
            newHead = start;

            start = nextNode;

            k--;
        }
        return newHead;
    }

}
