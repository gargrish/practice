package InterviewProblems;

import java.util.*;

/*
Input:
paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
banned = ["hit"]
Output: "ball"
Explanation:
"hit" occurs 3 times, but it is a banned word.
"ball" occurs twice (and no other word does), so it is the most frequent non-banned word in the paragraph.
Note that words in the paragraph are not case sensitive,
that punctuation is ignored (even if adjacent to words, such as "ball,"),
and that "hit" isn't the answer even though it occurs more because it is banned.
 */

public class MostCommonWord {
    public static String mostCommonWord(String paragraph, String[] banned) {
        String[] words = paragraph.toLowerCase().split("\\s|,|\\.");
        List<String> bannedList = Arrays.asList(banned);
        Map<String, Integer> counts = new HashMap<>();
        for (String word : words) {
            String w = word.toLowerCase().replaceAll("[^a-z]", "");
            if (!w.isBlank() && !bannedList.contains(w)) {
                counts.put(w, counts.getOrDefault(w, 0) + 1);
            }
        }
        List<Map.Entry<String, Integer>> list = new ArrayList<>(counts.entrySet());
        findTopWord(list, 0, list.size()-1);
        return list.get(0).getKey();
    }

    public static void findTopWord(List<Map.Entry<String, Integer>> list, int start, int end) {
        if(start>=end) {
            return;
        }
        int partition = partition(list, start, end);
        if(partition == 0) {
            return;
        }
        if(partition > 0) {
            findTopWord(list, start, partition-1);
        }else{
            findTopWord(list, partition+1, end);
        }
    }

    public static int partition(List<Map.Entry<String, Integer>> list, int start, int end){
        if(start >= end) {
            return start;
        }
        List<Map.Entry<String, Integer>> larger = new ArrayList<>();
        List<Map.Entry<String, Integer>> smaller = new ArrayList<>();
        Map.Entry<String, Integer> lastValue = list.get(end);
        for(int i=start;i<end;i++) {
            if(list.get(i).getValue() > list.get(end).getValue()) {
                larger.add(list.get(i));
            }else{
                smaller.add(list.get(i));
            }
        }
        for(Map.Entry<String, Integer> large: larger) {
            list.remove(start);
            list.add(start++, large);
        }
        int pivot = start;
        list.remove(start);
        list.add(start++, lastValue);
        for(Map.Entry<String, Integer> small: smaller) {
            list.remove(start);
            list.add(start++, small);
        }
        return pivot;
    }

    public static void main(String[] args) {
        String paragraph = "Bob. hIt, baLl";
        String[] banned = {"bob", "hit"};
        System.out.println(mostCommonWord(paragraph, banned));
    }
}
