package InterviewProblems;

import java.util.*;

public class ThreeSum {
    public static List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        Set<List<Integer>> outputSet = new LinkedHashSet<>();
        for (int i = 0; i < nums.length - 2; i++) {
            int k = nums.length - 1;
            for (int j = i + 1; j < nums.length - 1 && j < k; ) {
                if (nums[i] + nums[j] + nums[k] > 0) {
                    k--;
                } else if (nums[i] + nums[j] + nums[k] < 0) {
                    j++;
                } else {
                    outputSet.add(Arrays.asList(nums[i], nums[j], nums[k]));
                    j++;
                }
            }
        }
        return new ArrayList<>(outputSet);
    }

    public static void main(String[] args) {
        System.out.println(threeSum(new int[]{-1, 0, 1, 2, -1, -4}));

    }
}
