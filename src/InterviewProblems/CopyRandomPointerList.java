package InterviewProblems;

import java.util.HashMap;
import java.util.Map;

/*
Copy List with Random Pointer
 */

public class CopyRandomPointerList {
    static class Node {
        int val;
        Node next;
        Node random;

        public Node(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }
    }

    public static Node copyRandomList(Node head) {
        if (head == null) {
            return null;
        }
        Node start = head;
        Node deepCopy = new Node(start.val);
        Map<Node, Node> oldNewNodesMapping = new HashMap<>();
        Node deepCopyStart = deepCopy;
        oldNewNodesMapping.put(start, deepCopyStart);
        head = head.next;
        while (head != null) {
            deepCopy.next = new Node(head.val);
            deepCopy = deepCopy.next;
            oldNewNodesMapping.put(head, deepCopy);
            head = head.next;
        }
        deepCopy = deepCopyStart;
        head = start;
        while (deepCopy != null) {
            deepCopy.random = oldNewNodesMapping.get(head.random);
            head = head.next;
            deepCopy = deepCopy.next;
        }
        return deepCopyStart;
    }

    public static void main(String[] args) {
        Node head = new Node(7);
        head.random = null;
        head.next = new Node(13);
        head.next.next = new Node(11);
        head.next.next.next = new Node(10);
        head.next.next.next.next = new Node(1);
        head.next.random = head;
        head.next.next.random = head.next.next.next.next;
        head.next.next.next.random = head.next.next;
        head.next.next.next.next.random = head;

//        Node head = new Node(-1);
//        head.next = null;
//        head.random = head;
        Node copy = copyRandomList(head);
        System.out.println(copy.next.random);

    }
}
