package Cards;

public class Card {
    int number;
    Suit suit;

    Card(int number, Suit suit) {
        this.number = number;
        this.suit = suit;
    }

    @Override
    public String toString() {
        return this.number + " " + this.suit.toString();
    }
}
