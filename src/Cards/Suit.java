package Cards;

public enum Suit {
    SPADES,
    CLUBS,
    HEARTS,
    DIAMONDS
}
