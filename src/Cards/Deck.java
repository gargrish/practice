package Cards;

import java.util.*;

public class Deck {
    Card[] cards;

    Deck() {
        cards = new Card[52];
        int index = 0;
        for (Suit suit : Suit.values()) {
            for (int i = 1; i <= 13; i++) {
                cards[index++] = new Card(i, suit);
            }
        }
    }

    public void shuffle() {
        Random random = new Random();
        // shuffle cards
        for (int i = 0; i < cards.length; i++) {
            int next = random.nextInt(52);
            Card temp = cards[next];
            cards[next] = cards[i];
            cards[i] = temp;
        }
    }

    public Map<Player, Set<Card>> distribute(List<Player> players, int numberOfCards) {
        Map<Player, Set<Card>> playersCards = new HashMap<>();
        int cardIndex = 0;
        // return a map with cards for each player
        for (int i = 0; i < numberOfCards; i++) {
            for (int j = 0; j < players.size(); j++) {
                if (!playersCards.containsKey(players.get(j))) {
                    playersCards.put(players.get(j), new HashSet<>());
                }
                playersCards.get(players.get(j)).add(cards[cardIndex++]);
            }
        }
        cards = Arrays.copyOfRange(cards, cardIndex, cards.length);
        return playersCards;
    }


    public static void main(String[] args) {
        Deck deck = new Deck();
//        for (int i = 0; i < deck.cards.length; i++) {
//            System.out.println(deck.cards[i]);
//        }
//        System.out.println("After Shuffle------\n\n");
        deck.shuffle();
//        for (int i = 0; i < deck.cards.length; i++) {
//            System.out.println(deck.cards[i]);
//        }
        List<Player> players = new ArrayList<>();
        players.add(new Player("divya jaisawal"));
        players.add(new Player("sahil goel"));
        players.add(new Player("sahil ajmani"));
        players.add(new Player("rishabh garg"));
        System.out.println(deck.distribute(players, 2));
        System.out.println(deck.cards.length);
    }

}
