package Cards;

public class Player {
    String name;
    boolean isActive;

    Player(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
