package DataStructures.LinkedList;

public class LinkedListOperations {

    public void print(LinkedList list) {
        while (list != null) {
            System.out.printf("%d ", list.value);
            list = list.next;
        }
        System.out.println();
    }

    public LinkedList reverse(LinkedList head) {
        if (head.next == null) {
            return head;
        }
        LinkedList rest = head.next;
        LinkedList newHead = reverse(rest);
        rest.next = head;
        head.next = null;
        return newHead;
    }

    public void permutations(LinkedList head) {
        if (head != null) {
            System.out.printf("%d ", head.value);
            LinkedList rest = head.next;
            print(rest);
            permutations(rest);
        }
    }

    //Works for
    public static void main(String[] args) {
        LinkedList list = new LinkedList(1);
        list.next = new LinkedList(2);
        list.next.next = new LinkedList(3);
        list.next.next.next = new LinkedList(4);
//        list.next.next.next.next = new LinkedList(5);
//        list.next.next.next.next.next = new LinkedList(6);
//        list.next.next.next.next.next.next = new LinkedList(7);
        LinkedListOperations operations = new LinkedListOperations();
        operations.print(list);
        LinkedList newList = operations.reverse(list);
        System.out.println("-----");
        operations.print(newList);
        System.out.println("-----");
        operations.permutations(newList);
    }
}
