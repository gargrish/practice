package DataStructures.Trees;

public class NearestAncestor {

    public static TreeNode findAncestor(TreeNode root, int num1, int num2) {
        if (root == null) {
            return null;
        }
        if (root.value == num1 || root.value == num2) {
            return root;
        }
        TreeNode left = findAncestor(root.left, num1, num2);
        TreeNode right = findAncestor(root.right, num1, num2);
        if (left != null && right != null) {
            return root;
        }
        if (left != null) {
            if ((left.value == num1 && root.value == num2) || (left.value == num2 || root.value == num1)) {
                return root;
            }
            return left;
        }
        if (right != null) {
            if ((right.value == num1 && root.value == num2) || (right.value == num2 || root.value == num1)) {
                return root;
            }
            return right;
        }
        return null;
    }

    public static void main(String[] args) {
        TreeNode tree = TreeNode.init(6);
        System.out.println(findAncestor(tree, 1, 8));
        System.out.println(findAncestor(tree, 9, 8));
        System.out.println(findAncestor(tree, 4, 5));
    }
}
