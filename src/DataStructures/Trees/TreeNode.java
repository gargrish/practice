package DataStructures.Trees;

public class TreeNode {
    public TreeNode left, right;
    public int value;
    int childs;

    public TreeNode() {
        left = right = null;
    }

    public TreeNode(int x) {
        childs = 0;
        value = x;
        left = right = null;
    }

    @Override
    public String toString() {
        return value + "";
    }

    public static TreeNode init(int x) {
        TreeNode root = new TreeNode(x);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(1);
        root.left.right.right = new TreeNode(8);
        root.left.right.left = new TreeNode(9);
        root.right = new TreeNode(3);
        root.right.right = new TreeNode(5);
        root.right.right.right = new TreeNode(7);
        return root;
    }

    public static TreeNode init2(int x) {
        TreeNode root = new TreeNode(x);
        return root;
    }
}

