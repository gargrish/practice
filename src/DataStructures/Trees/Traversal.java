package DataStructures.Trees;

import java.util.ArrayList;
import java.util.List;

public class Traversal {

    public void preorder(TreeNode node) {
        if (node != null) {
            System.out.printf(" %d", node.value);
            preorder(node.left);
            preorder(node.right);
        }
    }

    public void inorder(TreeNode node) {
        if (node != null) {
            inorder(node.left);
            System.out.printf(" %d", node.value);
            inorder(node.right);
        }
    }

    public void postorder(TreeNode node) {
        if (node != null) {
            postorder(node.left);
            postorder(node.right);
            System.out.printf(" %d", node.value);
        }
    }

    public void levelOrder(TreeNode head, List<List<Integer>> levelOrderTraversal, int level) {
        if (head == null) {
            return;
        }
        if (levelOrderTraversal.size() <= level) {
            levelOrderTraversal.add(new ArrayList<>());
        }
        levelOrderTraversal.get(level).add(head.value);
        if (head.left == null && head.right == null) {
            return;
        }
        levelOrder(head.left, levelOrderTraversal, level + 1);
        levelOrder(head.right, levelOrderTraversal, level + 1);
    }

    public int totalNodeCounts(TreeNode node) {
        if (node == null) {
            return 0;
        }
        return totalNodeCounts(node.left) + totalNodeCounts(node.right) + 1;
    }

    public int calculateHeight(TreeNode node) {
        if (node == null) {
            return 0;
        }
        if (node.left == null && node.right == null) {
            return 1;
        }
        int leftVal = calculateHeight(node.left);
        int rightVal = calculateHeight(node.right);
        if (leftVal > rightVal) {
            return leftVal + 1;
        }
        return rightVal + 1;
    }

    public static void main(String[] args) {
        Traversal traversal = new Traversal();
        TreeNode tree = TreeNode.init(5);
        System.out.print("Preorder--->");
        traversal.preorder(tree);
        System.out.printf("\nInorder--->");
        traversal.inorder(tree);
        System.out.printf("\nPostorder--->");
        traversal.postorder(tree);
        System.out.printf("\nChild Node counts---> %d", traversal.totalNodeCounts(tree));
        System.out.printf("\nHeight of tree---> %d", traversal.calculateHeight(tree));
        TreeNode tree2 = TreeNode.init2(6);
        System.out.printf("\nHeight of tree2---> %d", traversal.calculateHeight(tree2));
        List<List<Integer>> levelOrder = new ArrayList<>();
        System.out.printf("\nLevelOrder ---> ");
        traversal.levelOrder(tree, levelOrder, 0);
        System.out.println(levelOrder);
    }
}
