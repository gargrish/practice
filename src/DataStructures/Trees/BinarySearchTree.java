package DataStructures.Trees;

public class BinarySearchTree {
    public void insert(TreeNode node, int value) {
        if (value < node.value) {
            if (node.left == null) {
                node.left = new TreeNode(value);
            } else {
                insert(node.left, value);
            }
        } else {
            if (node.right == null) {
                node.right = new TreeNode(value);
            } else {
                insert(node.right, value);
            }
        }
    }

    public void sort(TreeNode node) {
        if (node != null) {
            sort(node.left);
            System.out.printf("%d ", node.value);
            sort(node.right);
        }
    }

    public static void main(String[] args) {
        TreeNode tree = new TreeNode(8);
        BinarySearchTree bst = new BinarySearchTree();
        System.out.print("before insertion--> ");
        bst.sort(tree);
        bst.insert(tree, 6);
        System.out.print("after insertion--> ");
        bst.sort(tree);
        bst.insert(tree, 4);
        System.out.print("after insertion--> ");
        bst.sort(tree);
        bst.insert(tree, 10);
        System.out.print("after insertion--> ");
        bst.sort(tree);
        bst.insert(tree, 8);
        System.out.print("after insertion--> ");
        bst.sort(tree);
        bst.insert(tree, 2);
        System.out.print("after insertion--> ");
        bst.sort(tree);
    }
}
