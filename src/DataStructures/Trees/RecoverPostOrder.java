package DataStructures.Trees;

import java.util.Arrays;

public class RecoverPostOrder {

    public static TreeNode recover(TreeNode node, int[] preOrder, int[] inOrder) {
        if (node == null) {
            node = new TreeNode();
        }
        int rootInorderIndex = -1;
        for (int i = 0; i < inOrder.length; i++) {
            if (inOrder[i] == preOrder[0]) {
                rootInorderIndex = i;
                node.value = preOrder[0];
                preOrder = Arrays.copyOfRange(preOrder, 1, preOrder.length);
                break;
            }
        }
        if (rootInorderIndex == -1) {
            return null;
        }
        int[] leftInorder = new int[rootInorderIndex];
        for (int i = 0; i < rootInorderIndex; i++) {
            leftInorder[i] = inOrder[i];
        }
        int[] rightInorder = new int[inOrder.length - leftInorder.length - 1];
        int index = 0;
        for (int i = rootInorderIndex + 1; i < inOrder.length; i++) {
            rightInorder[index++] = inOrder[i];
        }
        int[] leftPreOrder = new int[leftInorder.length];
        int[] rightPreOrder = new int[rightInorder.length];
        int j;
        for (j = 0; j < leftInorder.length; j++) {
            leftPreOrder[j] = preOrder[j];
        }
        index = 0;
        for (int i = j; i < preOrder.length; i++) {
            rightPreOrder[index++] = preOrder[i];
        }
        node.left = recover(node.left, leftPreOrder, leftInorder);
        node.right = recover(node.right, rightPreOrder, rightInorder);
        return node;
    }

    public static void postOrderTraversal(TreeNode node) {
        if (node == null) {
            return;
        }
        postOrderTraversal(node.left);
        postOrderTraversal(node.right);
        System.out.println(node.value);
    }

    public static void preOrderTraversal(TreeNode node) {
        if (node == null) {
            return;
        }
        System.out.println(node.value);
        preOrderTraversal(node.left);
        preOrderTraversal(node.right);
    }

    public static void inOrderTraversal(TreeNode node) {
        if (node == null) {
            return;
        }
        inOrderTraversal(node.left);
        if (node.value != 0) {
            System.out.println("a " + node.value);
        }
        inOrderTraversal(node.right);
    }

    public static void main(String[] args) {
        int[] preOrder = new int[]{5, 2, 4, 1, 9, 8, 3, 10, 7};
        int[] inOrder = new int[]{4, 2, 9, 1, 8, 5, 3, 10, 7};
        TreeNode root = new TreeNode();
        recover(root, preOrder, inOrder);
        postOrderTraversal(root);
    }
}
