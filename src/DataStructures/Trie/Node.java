package DataStructures.Trie;

public class Node {
    String value;
    Node[] children;

    public Node() {
        this.children = new Node[26];
    }

    public Node(String value) {
        this.value = value;
        this.children = new Node[26];
    }
}
