package DataStructures.Trie;

import java.util.ArrayList;
import java.util.List;

public class Dictionary {
    public static void initialize(Node node) {
        Node aNode = new Node("");
        node.children['a' - 'a'] = aNode;
        Node acNode = new Node("");
        aNode.children['c' - 'a'] = acNode;
        Node actNode = new Node("act");
        acNode.children['t' - 'a'] = actNode;
        Node actiNode = new Node("acti");
        actNode.children['i' - 'a'] = actiNode;
        Node actioNode = new Node("actio");
        actiNode.children['o' - 'a'] = actioNode;
        Node actionNode = new Node("action");
        actioNode.children['n' - 'a'] = actionNode;
        Node abNode = new Node("ab");
        aNode.children['b' - 'a'] = abNode;
        Node abrNode = new Node("abr");
        abNode.children['r' - 'a'] = abrNode;
        Node abroNode = new Node("abro");
        abrNode.children['r' - 'a'] = abroNode;
        Node abroaNode = new Node("abroa");
        abroNode.children['r' - 'a'] = abroaNode;
        Node abroadNode = new Node("abroad");
        abroaNode.children['r' - 'a'] = abroadNode;

    }

    public static void startsWith(Node node, String s, List<Object> output) {
        if (node == null) {
            return;
        }
        if (node.value.contains(s)) {
            output.add(node.value);
        }
        for (int i = 0; i < node.children.length; i++) {
            if (node.children[i] != null) {
                startsWith(node.children[i], s, output);
            }
        }
    }

    public static void main(String[] args) {
        Node node = new Node("");
        initialize(node);
        System.out.println(node.children['a' - 'a'].children['c' - 'a'].children['t' - 'a'].value);
        List<Object> outList = new ArrayList<>();
        startsWith(node, "a", outList);
        System.out.println(outList);
    }
}
