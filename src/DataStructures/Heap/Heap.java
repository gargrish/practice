package DataStructures.Heap;

import java.util.*;

public class Heap {
    public static List<Integer> mergeLists(List<Integer>[] lists) {
        PriorityQueue<HeapObject> pQueue = new PriorityQueue<>((o1, o2) -> {
            if (o1.value < o2.value) {
                return -1;
            }
            if (o1.value > o2.value) {
                return 1;
            }
            return 0;
        });
        List<Integer> output = new ArrayList<>();
        int[] indexes = new int[lists.length];
        for (int i = 0; i < lists.length; i++) {
            pQueue.add(new HeapObject(i, lists[i].get(0)));
            indexes[i] = 1;
        }
        while (!pQueue.isEmpty()) {
            HeapObject temp = pQueue.poll();
            output.add(temp.value);
            int nextIndexToInsert = indexes[temp.index]++;
            if (nextIndexToInsert < lists[temp.index].size()) {
                pQueue.add(new HeapObject(temp.index, lists[temp.index].get(nextIndexToInsert)));
            }
        }
        return output;
    }

    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        List<Integer> list2 = new ArrayList<>();
        list2.add(1);
        list2.add(5);
        list2.add(6);
        List<Integer> list3 = new ArrayList<>();
        list3.add(1);
        list3.add(1);
        list3.add(1);
        list3.add(7);
        list3.add(8);
        List<Integer>[] lists = new ArrayList[3];
        lists[0] = list1;
        lists[1] = list2;
        lists[2] = list3;
        System.out.println("Merged List--->");
        System.out.println(mergeLists(lists));
    }
}
