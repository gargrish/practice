package DataStructures.Heap;

public class HeapObject {
    int index;
    int value;

    HeapObject() {
    }

    public HeapObject(int index, int value) {
        this.index = index;
        this.value = value;
    }
}
