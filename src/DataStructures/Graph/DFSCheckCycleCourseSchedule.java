package DataStructures.Graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*
There are a total of numCourses courses you have to take, labeled from 0 to numCourses-1.

Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?



Example 1:

Input: numCourses = 2, prerequisites = [[1,0]]
Output: true
Explanation: There are a total of 2 courses to take.
             To take course 1 you should have finished course 0. So it is possible.
Example 2:

Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
Output: false
Explanation: There are a total of 2 courses to take.
             To take course 1 you should have finished course 0, and to take course 0 you should
             also have finished course 1. So it is impossible.
 */

public class DFSCheckCycleCourseSchedule {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, Set<Integer>> prerequisitesEdges = new HashMap<>();
        for (int i = 0; i < prerequisites.length; i++) {
            if (!prerequisitesEdges.containsKey(prerequisites[i][0])) {
                prerequisitesEdges.put(prerequisites[i][0], new HashSet<>());
            }
            prerequisitesEdges.get(prerequisites[i][0]).add(prerequisites[i][1]);
        }
        Set<Integer> visited = new HashSet<>();
        for (int edge : prerequisitesEdges.keySet()) {
            if (!visited.contains(edge)) {
                if (!traverse(edge, prerequisitesEdges, visited, new HashSet<>())) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean traverse(int currentEdge, Map<Integer, Set<Integer>> prerequisitesEdges, Set<Integer> visited, Set<Integer> currentCoursePrerequisites) {
        if (currentCoursePrerequisites.contains(currentEdge)) {
            return false;
        }
        if (visited.contains(currentEdge)) {
            return true;
        }
        if (!prerequisitesEdges.containsKey(currentEdge)) {
            return true;
        }
        visited.add(currentEdge);
        for (int edge : prerequisitesEdges.get(currentEdge)) {
            currentCoursePrerequisites.add(currentEdge);
            if (!traverse(edge, prerequisitesEdges, visited, currentCoursePrerequisites)) {
                return false;
            }
            currentCoursePrerequisites.remove(currentEdge);
        }
        return true;
    }
}
