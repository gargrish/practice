package DataStructures.Graph;

import java.util.*;

public class Traversal {

    public static List<Integer> breadthFirstSearch(Map<Integer, List<Integer>> map) {
        Queue<Integer> queue = new LinkedList<>();
        List<Integer> output = new LinkedList<>();
        Set<Integer> visited = new HashSet<>();
        for (int key : map.keySet()) {
            if (visited.contains(key)) {
                continue;
            }
            queue.add(key);
            visited.add(key);
            output.add(key);
            while (!queue.isEmpty()) {
                for (int num : map.get(queue.remove())) {
                    if (visited.contains(num)) {
                        continue;
                    }
                    visited.add(num);
                    output.add(num);
                    queue.add(num);
                }
            }
        }
        return output;
    }

    public static List<Integer> depthFirstSearch(Map<Integer, List<Integer>> map) {
        List<Integer> output = new LinkedList<>();
        Set<Integer> visited = new HashSet<>();
        for (int num : map.keySet()) {
            if (!visited.contains(num)) {
                traverse(map, num, visited, output);
            }
        }
        return output;
    }

    public static List<Integer> topologicalTraversal(Map<Integer, List<Integer>> map) {
        Stack<Integer> stack = new Stack<>();
        Set<Integer> visited = new HashSet<>();
        for (int num : map.keySet()) {
            if (!visited.contains(num)) {
                traverse(map, num, visited, stack);
            }
        }
        List<Integer> output = new LinkedList<>();
        while (stack.size() > 0) {
            output.add(stack.pop());
        }
        return output;
    }

    public static void traverse(Map<Integer, List<Integer>> map, int start, Set<Integer> visited, List<Integer> output) {
        if (visited.contains(start)) {
            return;
        }
        output.add(start);
        visited.add(start);
        if (map.get(start).size() == 0) {
            return;
        }
        for (int currentNode : map.get(start)) {
            traverse(map, currentNode, visited, output);
        }
    }

    public static void main(String[] args) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        map.put(1, Arrays.asList(2, 3));
        map.put(2, Arrays.asList(6));
        map.put(3, Arrays.asList(4, 7, 8));
        map.put(4, Arrays.asList(5));
        map.put(5, Arrays.asList());
        map.put(6, Arrays.asList());
        map.put(7, Arrays.asList());
        map.put(8, Arrays.asList());
        System.out.println(breadthFirstSearch(map));
        System.out.println(depthFirstSearch(map));
        System.out.println(topologicalTraversal(map));

    }
}
