package DataStructures.Graph;

import java.util.*;

/*
Example 1:

Input: 2, [[1,0]]
Output: [0,1]
Explanation: There are a total of 2 courses to take. To take course 1 you should have finished
             course 0. So the correct course order is [0,1] .
Example 2:

Input: 4, [[1,0],[2,0],[3,1],[3,2]]
Output: [0,1,2,3] or [0,2,1,3]
Explanation: There are a total of 4 courses to take. To take course 3 you should have finished both
             courses 1 and 2. Both courses 1 and 2 should be taken after you finished course 0.
             So one correct course order is [0,1,2,3]. Another correct ordering is [0,2,1,3] .
 */

public class TopologicalSortCourseSchedule2 {
    public int[] findOrder(int numCourses, int[][] prerequisites) {

        int[] courses = new int[numCourses];

        Set<Integer> independentCourses = new HashSet<>();
        for (int i = 0; i < numCourses; i++) {
            independentCourses.add(i);
            courses[i] = i;
        }

        // if no dependency, then return all the courses
        if (prerequisites.length == 0) {
            return courses;
        }

        Map<Integer, Set<Integer>> prerequisitesGraph = new HashMap<>();
        for (int i = 0; i < prerequisites.length; i++) {
            // remove courses which have dependencies
            independentCourses.remove(prerequisites[i][0]);
            independentCourses.remove(prerequisites[i][1]);
            if (!prerequisitesGraph.containsKey(prerequisites[i][0])) {
                prerequisitesGraph.put(prerequisites[i][0], new HashSet<>());
            }
            prerequisitesGraph.get(prerequisites[i][0]).add(prerequisites[i][1]);
        }

        Set<Integer> visited = new HashSet<>();
        Set<Integer> coursesOrder = new LinkedHashSet<>();
        for (int course : prerequisitesGraph.keySet()) {
            if (visited.contains(course)) {
                continue;
            }
            if (!traverse(course, prerequisitesGraph, coursesOrder, visited, new HashSet<>())) {
                return new int[]{};
            }
        }

        if (coursesOrder.size() == 0) {
            return new int[]{0};
        }

        int[] finalOrder = new int[numCourses];
        int index = 0;
        for (int course : coursesOrder) {
            finalOrder[index++] = course;
        }
        for (int course : independentCourses) {
            finalOrder[index++] = course;
        }
        return finalOrder;
    }

    public boolean traverse(int currentCourse, Map<Integer, Set<Integer>> prerequisitesGraph, Set<Integer> coursesOrder, Set<Integer> visited, Set<Integer> currentCourseOrder) {
        if (currentCourseOrder.contains(currentCourse)) {
            return false;
        }
        if (visited.contains(currentCourse)) {
            return true;
        }

        visited.add(currentCourse);

        if (!prerequisitesGraph.containsKey(currentCourse)) {
            return true;
        }

        for (int course : prerequisitesGraph.get(currentCourse)) {
            currentCourseOrder.add(currentCourse);
            if (!traverse(course, prerequisitesGraph, coursesOrder, visited, currentCourseOrder)) {
                return false;
            }
            coursesOrder.add(course);
            currentCourseOrder.remove(currentCourse);
        }
        coursesOrder.add(currentCourse);
        return true;
    }
}
