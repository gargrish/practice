package AmazonInterview;// IMPORT LIBRARY PACKAGES NEEDED BY YOUR PROGRAM
// SOME CLASSES WITHIN A PACKAGE MAY BE RESTRICTED
// DEFINE ANY CLASS AND METHOD NEEDED

import java.util.*;

// CLASS BEGINS, THIS CLASS IS REQUIRED
class Solution {
    // METHOD SIGNATURE BEGINS, THIS METHOD IS REQUIRED
    public static ArrayList<String> popularNToys(int numToys,
                                                 int topToys,
                                                 List<String> toys,
                                                 int numQuotes,
                                                 List<String> quotes) {
        Map<String, Integer> occurences = new HashMap<>();
        ArrayList<String> output = new ArrayList<>();
        for (int i = 0; i < numToys; i++) {
            occurences.put(toys.get(i), 0);
        }
        for (int i = 0; i < numToys; i++) {
            for (String quote : quotes) {
                String[] arr = quote.toLowerCase().split(" ");
                Set<String> tempSet = new HashSet(Arrays.asList(arr));
                if (tempSet.contains(toys.get(i).replaceAll("[^a-zA-Z1-9 ]", "").toLowerCase())) {
                    occurences.put(toys.get(i), occurences.get(toys.get(i)) + 1);
                }
            }
        }
        if (topToys > numToys) {
            for (String key : occurences.keySet()) {
                if (occurences.get(key) > 0) {
                    output.add(key);
                }
            }
            return output;
        }
        List<Map.Entry<String, Integer>> list =
                new ArrayList<>(occurences.entrySet());
        // Sort the list
        Collections.sort(list, (o1, o2) -> {
            if (o1.getValue() < o2.getValue()) {
                return 1;
            }
            if (o1.getValue() > o2.getValue()) {
                return -1;
            }
            return o1.getKey().toLowerCase().compareTo(o2.getKey().toLowerCase());
        });

        // put data from sorted list to hashmap
        HashMap<String, Integer> temp = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> occurence : list) {
            temp.put(occurence.getKey(), occurence.getValue());
        }
        ArrayList<String> out2 = new ArrayList<>(temp.keySet());
        for (int i = 0; i < topToys; i++) {
            output.add(out2.get(i));
        }
        return output;
    }
    // METHOD SIGNATURE ENDS

    public static void main(String[] args) {
        List<String> toys = Arrays.asList("puppy", "teddy", "paw", "peppa");
        List<String> quotes = Arrays.asList("puppy is a swjsdw sws", "swsw swsww puppy paw sw teddy.", "wdwdq qdwqwdpaw dwedqewd qwdqd", "peppa dqwqddqw qwdwq paw");
        System.out.println(popularNToys(4, 3, toys, 4, quotes));
    }


}