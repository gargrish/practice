package AmazonInterview;

import java.util.*;

public class SortList {

    public static List<String> sort(List<String> input) {
        Collections.sort(input, (list1, list2) -> {
            boolean isNumericL1 = containsNumbers(list1);
            boolean isNumericL2 = containsNumbers(list2);

            if (!isNumericL1 && isNumericL2) {
                return -1;
            }
            if (isNumericL1 && !isNumericL2) {
                return 1;
            }
            int compare1 = list1.substring(0, list1.indexOf(' ')).compareTo(list2.substring(0, list2.indexOf(' ')));
            if (!isNumericL1) {
                int compare2 = list1.substring(list1.indexOf(' ') + 1).compareTo(list2.substring(list2.indexOf(' ') + 1));
                if (compare2 == -1) {
                    return -1;
                }
                if (compare2 == 1) {
                    return 1;
                }
                return compare1;
            }
            if (isNumericL1 && isNumericL2) {
                return compare1;
            }
            return -1;
        });
        return input;
    }

    private static boolean containsNumbers(String list) {
        String value = list.substring(list.indexOf(' ') + 1);
        char[] chars = value.toLowerCase().toCharArray();
        for (char ch : chars) {
            if (!(ch >= 'a' && ch <= 'z') && ch != ' ') {
                System.out.println(value);
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        List<String> input = Arrays.asList("Id1 abc def", "Id3 abc dgh", "Id4 623 556", "Id5 145 23ab", "Id6 abb efg", "Id2 abc dgh", "Id5 145 23ab");
        System.out.println(sort(input));
    }
}
