package Recursion;

public class Fibonacci {
    public static void main(String[] args) {
        Fibonacci f = new Fibonacci();
        for (int i = 0; i < 10; i++) {
            System.out.printf("%d ", f.fibonacci(i));
        }
    }

    public int fibonacci(int n) {
        if (n < 2) {
            return n;
        }
        return fibonacci(n - 2) + fibonacci(n - 1);
    }
}
