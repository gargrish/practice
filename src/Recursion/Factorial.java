package Recursion;

public class Factorial {
    public static void main(String[] args) {
        Factorial f = new Factorial();
        System.out.println(f.factorial(0));
        System.out.println(f.factorial(2));
        System.out.println(f.factorial(5));
    }

    public int factorial(int n) {
        if (n < 2) {
            return 1;
        }
        return n * factorial(n - 1);
    }
}
