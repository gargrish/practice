package Recursion;

public class Power {
    public static void main(String[] args) {
        Power f = new Power();
        int x = 5;
        int n = 10;
        int pow1 = f.power(x, n);
        int pow2 = f.optimizedPower(x, n);
        System.out.println(pow1);
        System.out.println(pow2);
        if (pow1 != pow2) {
            System.out.println("Wrong result");
        }
    }

    // Basic Recursive Function that calculates power
    public int power(int x, int n) {
        if (n < 2) {
            return x;
        }
        return x * power(x, n - 1);
    }

    // Recursive Function calculates power in log(n)
    public int optimizedPower(int x, int n) {
        if (n < 2) {
            return x;
        }
        if (n % 2 == 0) {
            int temp = optimizedPower(x, n / 2);
            return temp * temp;
        }
        int temp = optimizedPower(x, n / 2);
        return temp * temp * x;
    }
}
