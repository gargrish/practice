package Recursion.Permutations;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Chess {

    public static void queenPlacement(int index, Set<List<Integer>> queensSet, List<Integer> list) {
        if (list.size() == 8) {
            queensSet.add(new LinkedList<>(list));
            return;
        }
        for (int i = 0; i < 8; i++) {
            if (doCut(index, i, list)) {
                continue;
            }
            list.add(i);
            queenPlacement(index + 1, queensSet, list);
            list.remove(list.size() - 1);
        }
    }

    public static boolean doCut(int x, int y, List<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            if (x == i || y == list.get(i) || Math.abs(x - i) == Math.abs(y - list.get(i))) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Set<List<Integer>> queensSet = ConcurrentHashMap.newKeySet();
        List<Integer> list = new LinkedList<>();
        queenPlacement(0, queensSet, list);
        System.out.println(queensSet);
    }
}
