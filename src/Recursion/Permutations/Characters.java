package Recursion.Permutations;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Characters {

    public static void words(Map<Character, Integer> characterIntegerHashMap) {
        int length = 0;
        for (Integer i : characterIntegerHashMap.values()) {
            length += i;
        }
        Set<String> output = new LinkedHashSet<>();
        wordsUtil(characterIntegerHashMap, "", output, length);
        System.out.println(output);
    }

    public static void wordsUtil(Map<Character, Integer> characterIntegerMap, String selected, Set<String> output, int length) {
        if (selected.length() == length) {
            output.add(selected);
            return;
        }
        for (Character ch : characterIntegerMap.keySet()) {
            if (characterIntegerMap.get(ch) == 0) {
                continue;
            }
            characterIntegerMap.put(ch, characterIntegerMap.get(ch) - 1);
            wordsUtil(characterIntegerMap, selected + ch, output, length);
            characterIntegerMap.put(ch, characterIntegerMap.get(ch) + 1);
        }
    }

    public static void main(String[] args) {
        Map<Character, Integer> hashMap = new ConcurrentHashMap<>();
        hashMap.put('a', 3);
        hashMap.put('b', 2);
        hashMap.put('c', 2);
        words(hashMap);
    }
}
