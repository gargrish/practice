package Recursion.Permutations;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Numbers {
    public static void permutation(Set<Integer> numbers) {
        permutationUtil(numbers, "", numbers.size());
    }

    public static void permutationUtil(Set<Integer> availableNumbers, String selected, int length) {
        if (selected.length() == length) {
            System.out.println(selected);
            return;
        }
        for (int availableNumber : availableNumbers) {
            availableNumbers.remove(availableNumber);
            permutationUtil(availableNumbers, selected + availableNumber, length);
            availableNumbers.add(availableNumber);
        }
    }

    public static void main(String[] args) {
        Set<Integer> set = ConcurrentHashMap.newKeySet();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        Numbers.permutation(set);
    }
}
