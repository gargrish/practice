package Recursion.Permutations;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Dictionary {
    public static void longestChain(Set<String> words, Map<String, Integer> output) {
        if (words.size() == 0) {
            return;
        }
        for (String word : words) {
            util(words, word, output);
        }
    }

    public static void util(Set<String> words, String word, Map<String, Integer> output) {
        if (word.length() <= 1) {
            return;
        }
        if (output.get(word) != null) {
            return;
        }
        for (int i = 0; i < word.toCharArray().length; i++) {
            StringBuilder wordSb = new StringBuilder(word);
            String temp = wordSb.deleteCharAt(i).toString();
            if (words.contains(temp)) {
                if (temp.length() > 1) {
                    util(words, temp, output);
                }
                int wordVal = 0;
                int tempVal = 0;
                if (output.containsKey(word)) {
                    wordVal = output.get(word);
                }
                if (output.containsKey(temp)) {
                    tempVal = output.get(temp);
                }
                output.put(word, Math.max(wordVal, 1 + tempVal));
            }
        }
    }

    public static void main(String[] args) {
        Set<String> words = ConcurrentHashMap.newKeySet();
        words.add("abcd");
        words.add("bcd");
        words.add("acd");
        words.add("abd");
        words.add("bd");
        words.add("cd");
        words.add("efgh");
        words.add("d");
        words.add("e");
        words.add("eg");
        words.add("egh");
        words.add("abcde");
        Map<String, Integer> output = new ConcurrentHashMap<>();
        longestChain(words, output);
        for (String a : output.keySet()) {
            System.out.println(a + " " + output.get(a));
        }
    }
}
