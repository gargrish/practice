package Recursion.Permutations;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Permutations {
    public static void permutations(Set<Integer> set, String prefix, int length) {
        if (prefix.length() == length) {
            System.out.println(prefix);
            return;
        }
        for (Integer i : set) {
            set.remove(i);
            permutations(set, prefix + i, length);
            set.add(i);
        }
    }

    public static void stringPermutations(Map<Character, Integer> map, String selected, Set<String> output, int length) {
        if (selected.length() == length) {
            output.add(selected);
            return;
        }
        for (Character ch : map.keySet()) {
            if (map.get(ch) == 0) {
                continue;
            }
            map.put(ch, map.get(ch) - 1);
            stringPermutations(map, selected + ch, output, length);
            map.put(ch, map.get(ch) + 1);
        }
    }

    public static void dictionary(Map<String, Integer> wordsMap) {
        for (String s : wordsMap.keySet()) {
            dictionaryUtil(wordsMap, s);
        }
    }

    public static void dictionaryUtil(Map<String, Integer> wordsMap, String word) {
        if (word.length() <= 1) {
            return;
        }
        for (int i = 0; i < word.toCharArray().length; i++) {
            StringBuilder sb = new StringBuilder(word);
            String temp = sb.deleteCharAt(i).toString();
            if (wordsMap.keySet().contains(temp)) {
                if (temp.length() > 1) {
                    dictionaryUtil(wordsMap, temp);
                }
                wordsMap.put(word, Math.max(wordsMap.get(word), 1 + wordsMap.get(temp)));
            }
        }
    }

    public static void queenPlacement(int index, Set<List<Integer>> queensPlacement, List<Integer> list) {
        if (list.size() == 8) {
            queensPlacement.add(new LinkedList<>(list));
            return;
        }
        for (int i = 0; i < 8; i++) {
            if (doCut(index, i, list)) {
                continue;
            }
            list.add(i);
            queenPlacement(index + 1, queensPlacement, list);
            list.remove(list.size() - 1);
        }
    }

    public static boolean doCut(int x, int y, List<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            if (x == i || y == list.get(i) || Math.abs(x - i) == Math.abs(y - list.get(i))) {
                return true;
            }
        }
        return false;
    }

    public static void wordPermutations(String word, String prefix, int length, Set<String> wordPermutations) {
        if (prefix.length() == length) {
            wordPermutations.add(prefix);
            return;
        }
        StringBuilder sb = new StringBuilder(word);
        for (int i = 0; i < word.length(); i++) {
            char ch = sb.charAt(i);
            String temp = sb.deleteCharAt(i).toString();
            //sb = new StringBuilder(word);
            wordPermutations(temp, prefix + ch, length, wordPermutations);
        }
    }

    //5, 6, -7, -6, 8, -1, 7
    public static int maxSum(int[] numbers) {
        int prevSum = 0;
        int sum = 0;
        int max = 0;
        for (int i = 0; i < numbers.length; i++) {
            prevSum = sum;
            sum += numbers[i];
            if (prevSum > sum && prevSum > max) {
                max = prevSum;
            } else if (sum > max) {
                max = sum;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        Set<Integer> set = ConcurrentHashMap.newKeySet();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        permutations(set, "", 4);

        Map<Character, Integer> map = new ConcurrentHashMap<>();
        map.put('a', 3);
        map.put('b', 2);
        int length = 0;
        Set<String> output = ConcurrentHashMap.newKeySet();
        for (Character ch : map.keySet()) {
            length += map.get(ch);
        }
        stringPermutations(map, "", output, length);
        System.out.println(output);

        Map<String, Integer> longestChainMap = new ConcurrentHashMap<>();
        longestChainMap.put("abcd", 0);
        longestChainMap.put("acd", 0);
        longestChainMap.put("ad", 0);
        longestChainMap.put("ac", 0);
        longestChainMap.put("d", 0);
        longestChainMap.put("efgh", 0);
        longestChainMap.put("efg", 0);
        longestChainMap.put("fg", 0);
        dictionary(longestChainMap);
        System.out.println(longestChainMap);

        Set<List<Integer>> response = ConcurrentHashMap.newKeySet();
        List<Integer> list = new LinkedList<>();
        queenPlacement(0, response, list);
        System.out.println(response);

        Set<String> wordPermutations = ConcurrentHashMap.newKeySet();
        wordPermutations("aacd", "", 4, wordPermutations);
        System.out.println(wordPermutations);

        int[] numbers = new int[]{5, 6, -7, -6, 8, -1, 7};
        System.out.println(maxSum(numbers));
        int[] numbers2 = new int[]{5, 6, 3, -7, 30, -1, -2};
        System.out.println(maxSum(numbers2));
    }
}
