package Recursion;

public class PolynomialSeries {
    public static void main(String[] args) {
        PolynomialSeries f = new PolynomialSeries();
        int[] constants = new int[]{3, 4, 5, 6, 7};
        System.out.println(f.sum(constants, 2, 5));
    }

    // Expression: (A+Bx+Cx2+Dx3+....)
    public double sum(int[] constants, int x, int n) {
        if (n < 1) {
            return 0;
        }
        System.out.println(sum(constants, x, n - 1) + constants[n - 1] * Math.pow(x, n - 1));
        return sum(constants, x, n - 1) + constants[n - 1] * Math.pow(x, n - 1);
    }
}
