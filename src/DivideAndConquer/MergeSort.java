package DivideAndConquer;

public class MergeSort {
    public static void merge(int[] arr, int start, int mid, int end) {
        if (start == end) {
            return;
        }
        int[] out = new int[end - start + 1];
        int index = 0;
        int i = start;
        int j = mid + 1;
        while (i <= mid && j < (end - start + 1)) {
            if (arr[i] <= arr[j]) {
                out[index++] = arr[i++];
            } else {
                out[index++] = arr[j++];
            }
        }
        while (i <= mid) {
            out[index++] = arr[i++];
        }
        while (j < end - start + 1) {
            out[index++] = arr[j++];
        }
        for (int y = start, z = 0; y < end - start + 1; y++, z++) {
            arr[y] = out[z];
        }
    }

    // 10 12 6 8 7 9 5 3
    public static void mergeSort(int[] arr, int start, int end) {
        if (arr.length == 0) {
            return;
        }
        if (start >= end) {
            return;
        }
        int mid = start + (end - start) / 2;
        mergeSort(arr, start, mid);
        mergeSort(arr, mid + 1, end);
        merge(arr, start, mid, end);
    }

    public static void main(String[] args) {
        int[] arr = new int[]{3, 6, 7, 9, 6, 8, 10, 12};
        mergeSort(arr, 0, arr.length - 1);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
