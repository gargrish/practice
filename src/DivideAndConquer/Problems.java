package DivideAndConquer;

import java.util.LinkedList;
import java.util.List;

public class Problems {

    public static int findKthLargest(int[] arr, int k) {
        divide(arr, 0, arr.length - 1, k);
        return arr[k - 1];
    }

    public static void divide(int[] arr, int start, int end, int k) {
        if (start >= end) {
            return;
        }
        int partition = partition(arr, start, end);
        if (partition == k - 1) {
            return;
        }
        if (partition < k - 1) {
            divide(arr, partition + 1, end, k);
        } else {
            divide(arr, start, partition - 1, k);
        }
    }

    public static int partition(int[] arr, int start, int end) {
        if (start >= end) {
            return start;
        }
        List<Integer> larger = new LinkedList<>();
        List<Integer> smaller = new LinkedList<>();
        for (int i = start; i < end; i++) {
            if (arr[i] > arr[end]) {
                larger.add(arr[i]);
            } else {
                smaller.add(arr[i]);
            }
        }
        for (int large : larger) {
            arr[start++] = large;
        }
        int pivot = start;
        arr[start++] = arr[end];
        for (int small : smaller) {
            arr[start++] = small;
        }
        return pivot;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1, 5, 2, 10, 0, 30, 21};
        int[] arr2 = new int[]{1, 5, 2, 10, 0, 30};
//        [1, 5, 2, 10, 0, 30, 21], 1 => 30
//                [1, 5, 2, 10, 0, 30, 21], 7 => 0
//
//                [1, 5, 2, 10, 0, 30], 1 => 30
//                [1, 5, 2, 10, 0, 30], 6 =>   0
        System.out.println(findKthLargest(arr2, 1));
        System.out.println(findKthLargest(arr2, 6));
    }
}
