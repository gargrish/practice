package DivideAndConquer;

import java.util.LinkedList;
import java.util.List;

public class QuickSort {

    public static int partitionMemory(int[] arr, int start, int end) {
        if (start >= end) {
            return start;
        }
        short x = (short)start;
        List<Integer> smaller = new LinkedList<>();
        List<Integer> larger = new LinkedList<>();
        for (int i = start; i < end; i++) {
            if (arr[i] > arr[end]) {
                larger.add(arr[i]);
            } else {
                smaller.add(arr[i]);
            }
        }
        for (int small : smaller) {
            arr[start++] = small;
        }
        int pivot = start;
        arr[start++] = arr[end];
        for (int large : larger) {
            arr[start++] = large;
        }
        return pivot;
    }

    public static int partition(int[] arr, int start, int end) {
        if (start >= end) {
            return start;
        }
        int i = start;
        int j = end - 1;
        while (i < j) {
            while (arr[i] < arr[end] && i < end) {
                i++;
            }
            while (arr[j] > arr[end] && j > i) {
                j--;
            }
            if (i > j) {
                return i;
            }
            if (i == j && i < end && arr[i] > arr[end]) {
                int temp = arr[i];
                arr[i] = arr[end];
                arr[end] = temp;
                return i;
            } else if (i == j && i < end && arr[i] < arr[end]) {
                return end;
            } else {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        if (arr[i] > arr[end]) {
            int temp = arr[i];
            arr[i] = arr[end];
            arr[end] = temp;
            return end;
        }
        return i;
    }

    public static void quickSort(int[] arr, int start, int end) {
        if (start >= end) {
            return;
        }
        int pivot = partition(arr, start, end);
//        System.out.println(pivot);
        quickSort(arr, start, pivot - 1);
        quickSort(arr, pivot + 1, end);

    }

    public static void quickSortMemory(int[] arr, int start, int end) {
        if (start >= end) {
            return;
        }
        int pivot = partitionMemory(arr, start, end);
//        System.out.println(pivot);
        quickSort(arr, start, pivot - 1);
        quickSort(arr, pivot + 1, end);

    }

    public static void main(String[] args) {
        int[] arr1 = new int[]{3, 6, 7, 4, 12, 8, 10, 9, 8, 6, 13, 5, 4, 3};
        int[] arr2 = new int[]{3, 6, 7, 4, 12, 8, 10, 9, 8, 6, 13, 5, 4, 3};
        //int[] arr = new int[]{10, 9, 8, 6, 13, 5, 4};
        //int[] arr = new int[]{3, 6, 7, 4, 2, 1};
        //int[] arr = new int[]{8, 7, 4, 2, 1};
        quickSortMemory(arr1, 0, arr1.length - 1);
        for (int i = 0; i < arr1.length; i++) {
            System.out.println(arr1[i]);
        }
        quickSort(arr2, 0, arr1.length - 1);
        for (int i = 0; i < arr2.length; i++) {
            System.out.println(arr2[i]);
        }
    }
}
